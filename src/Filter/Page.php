<?php

namespace Filter;

use AEngine\Orchid\Filter\AbstractFilter;
use AEngine\Orchid\Filter\TraitFilter;

class Page extends AbstractFilter
{
    use TraitFilter;

    public static function checkUri(array &$data)
    {
        $filter = new self($data);

        return $filter->attr('url')->addRule($filter->leadEscape())->run();
    }

}
