<?php

namespace Collection;

use AEngine\Orchid\Collection;
use Db;
use PDO;


class Pages extends Collection
{

    // protected static $model = 'Model\Page';

    public static function fetch(array $data = [])
    {
        $default = [
            "deleted" => false,
            "limit" => 100,
            "offset" => 0,

        ];
        $data = array_merge($default, $data);

        $where = " 1 ";
        $limit = "";

        if (!$data["deleted"]) {
            $where .= " AND !`deleted`";
        }
        if ($data["limit"]) {
            $limit .= "LIMIT " . $data["limit"];
        }
        if ($data["offset"]) {
            $limit .= "OFFSET " . $data["offset"];
        }


        $sth = Db::query("SELECT * FROM `page` WHERE {$where} {$limit}");
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);


        return new static(is_array($result) ? $result : []);
    }


}