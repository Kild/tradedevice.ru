<?php

namespace Collection;

use AEngine\Orchid\Collection;
use Db;
use Mem;
use PDO;

class News extends Collection
{

    // protected static $model = "\\Page\\ModelPage";

    public static function fetch(array $data = [])
    {
        $default = [
            "deleted"        => false,
            "limit"          => 100,
            "offset"         => 0,
            "orderBy"        => "id",
            "orderDirection" => "desc",
        ];
        $data = array_merge($default, $data);

        $result = Mem::get('news:list', false);

        if ($result === false) {
            $where = " 1 ";
            $order = "";
            $limit = "";

            if (!$data["deleted"]) {
                $where .= " AND !`deleted`";
            }
            if ($data["limit"]) {
                $limit .= "LIMIT " . $data["limit"];
            }
            if ($data["offset"]) {
                $limit .= "OFFSET " . $data["offset"];
            }
            if ($data["orderBy"]) {
                $order = "ORDER BY " . $data["orderBy"];

                if (in_array($data["orderDirection"], ["desc", "asc"])) {
                    $order .= " " . $data["orderDirection"];
                }
            }

            $sth = Db::query("SELECT * FROM `news` WHERE {$where} {$order} {$limit}");
            $result = $sth->fetchAll(PDO::FETCH_ASSOC);


            Mem::set('news:list', $result, 60 * 60);
        }

        return new static(is_array($result) ? $result : []);
    }


}