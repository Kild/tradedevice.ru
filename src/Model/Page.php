<?php

namespace Model;

use AEngine\Orchid\Model;
use Db;
use Mem;
use PDO;

class Page extends Model
{
    protected static $field = [
        'id'       => false,
        'url'      => false,
        'title'    => false,
        'content'  => false,
        'template' => false,
        'create'   => 0,
        'update'   => 0,
        'deleted'  => false,
    ];


    public static function fetch(array $data = [])
    {
        $default = [
            'id'      => false,
            'url'     => false,
            'deleted' => false,
        ];
        $data = array_merge($default, $data);

        $result = Mem::get('page:' . $data['url'], false);

        if ($result === false) {
            $where = [1];

            if ($data["id"]) {
                $where[] = "`id` = '" . $data["id"] . "'";
            }
            if ($data["url"]) {
                $where[] = "`url` = '" . $data["url"] . "'";
            }
            if (!$data["deleted"]) {
                $where[] = "!`deleted`";
            }

            $sth = Db::query("SELECT * FROM `page` WHERE " . implode(' AND ', $where) . " LIMIT 1");
            $result = $sth->fetch(PDO::FETCH_ASSOC);

            Mem::set('page:' . $data['url'], $result, 60 * 60);
        }

        return new static(is_array($result) ? $result : []);
    }
}

