<?php

namespace Model;

use AEngine\Orchid\Model;
use Db;
use Mem;
use PDO;

class News extends Model
{
    protected static $field = [
        "id"       => false,
        "url"      => false,
        "title"    => false,
        "announce" => false,
        "text"     => false,
        "img"      => false,
        "video"    => false,
        "create"   => 0,
        "update"   => 0,
        "deleted"  => false,
    ];

    public static function fetch(array $data = [])
    {
        $default = [
            "id"      => false,
            "url"     => false,
            "deleted" => false,
        ];
        $data = array_merge($default, $data);

        $result = Mem::get('news:'.$data["url"] , false);

        if ($result === false) {
            $where = " 1 ";

            if ($data["id"]) {
                $where .= " AND `id` = '" . $data["id"] . "'";
            }
            if ($data["url"]) {
                $where .= " AND `url` = '" . $data["url"] . "'";
            }
            if (!$data["deleted"]) {
                $where .= " AND !`deleted`";
            }

            $sth = Db::query("SELECT * FROM `news` WHERE {$where} LIMIT 1");
            $result = $sth->fetch(PDO::FETCH_ASSOC);

            Mem::set('news:'.$data["url"], $result, 60 * 60);
        }



        return new static(is_array($result) ? $result : []);
    }
}

