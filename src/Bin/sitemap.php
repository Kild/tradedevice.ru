<?php

use AEngine\Orchid\App;
use samdark\sitemap\Sitemap;


require_once(__DIR__ . '/../../instance.php');

$app = App::getInstance();

$map = new Sitemap($app->getBaseDir() . "/www/sitemap.xml");

$map->addItem("https://" . $app->getBaseHost(), time(), Sitemap::DAILY, 1);

/* обработка страниц */
$page = \Collection\Pages::fetch();

foreach ($page as $row) {
    $address = "https://" . $app->getBaseHost() . "/" . $row["url"];
    $map->addItem($address, strtotime($row["update"]), Sitemap::DAILY, 1);
}

/* обработка новостей */
//$map->addItem("https://" . $app->getBaseHost() . '/news', time(), Sitemap::MONTHLY, 0.8);
//$page = \Collection\News::fetch();
//
//foreach ($page as $row) {
//    $address = "https://" . $app->getBaseHost() . "/news/" . $row["url"];
//    $map->addItem($address, strtotime($row["update"]), Sitemap::MONTHLY, 0.8);
//}

/* обработка каталога */
$map->addItem("https://" . $app->getBaseHost() . '/catalog', time(), Sitemap::WEEKLY, 1);

/* обработка категорий */
$katalogAll =  \TradeMaster::getKatalog();
$katalog = $katalogAll->find('idParent', 0);

foreach ($katalog as $row) {

    $address = "https://" . $app->getBaseHost() . "/catalog/" . $row["link"];
    $map->addItem($address, $row["changeDate"] ? strtotime($row["changeDate"]) : null, Sitemap::WEEKLY, 1);

    /* обработка товаров */
    $list = \TradeMaster::getItemList(["ih" => $row["link"], "do" => 1000, 'allTovar' => false]);
    if (count($list) > 0) {
        $tovarLink = '';
        foreach ($list as $item) {
            if ($tovarLink != $item["link"]) {
                $addressItem = $address . "/" . $item["link"];
                $map->addItem($addressItem, $item["changeDate"] ? strtotime($item["changeDate"]) : null, Sitemap::WEEKLY, 1);
                $tovarLink = $item["link"];
            }
        }
    }

    $katalogSub = $katalogAll->find('idParent', $row["idZvena"]);
    if (count($katalogSub) > 0) {
        foreach ($katalogSub as $rowSub) {

            $addressSub = $address . "/" . $rowSub["link"];
            $map->addItem($addressSub, $rowSub["changeDate"] ? strtotime($rowSub["changeDate"]) : null, Sitemap::WEEKLY, 1);

            $list = \TradeMaster::getItemList(["ih" => $rowSub["link"], "do" => 1000, 'allTovar' => false]);
            if (count($list) > 0) {
                $tovarLink = '';
                foreach ($list as $item) {
                    if ($tovarLink != $item["link"]) {
                        $addressItem = $addressSub . "/" . $item["link"];
                        $map->addItem($addressItem, $item["changeDate"] ? strtotime($item["changeDate"]) : null, Sitemap::WEEKLY, 1);
                        $tovarLink = $item["link"];
                    }
                }
            }

            $katalogSubSub = $katalogAll->find('idParent', $rowSub["idZvena"]);
            if (count($katalogSubSub) > 0) {

                foreach ($katalogSubSub as $rowSubSub) {

                    $addressSubSub = $addressSub . "/" . $rowSubSub["link"];
                    $map->addItem($addressSubSub, $rowSubSub["changeDate"] ? strtotime($rowSubSub["changeDate"]) : null, Sitemap::WEEKLY, 1);


                    $list = \TradeMaster::getItemList(["ih" => $rowSubSub["link"], "do" => 1000, 'allTovar' => false]);
                    if (count($list) > 0) {
                        $tovarLink = '';
                        foreach ($list as $item) {
                            if ($tovarLink != $item["link"]) {
                                $addressItem = $addressSubSub . "/" . $item["link"];


                                    $map->addItem($addressItem, $item["changeDate"] ? strtotime($item["changeDate"]) : null, Sitemap::WEEKLY, 1);





                                $tovarLink = $item["link"];
                            }
                        }
                    }
                }
            }
        }
    }

}

$map->write();
