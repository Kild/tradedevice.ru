<?php


// yml?kateg=kateg - yml для категории


use AEngine\Orchid\App;
use AEngine\Orchid\Misc\Str;
use yml\TM_Yml;

$kateg= '';



if (!$argv[1]) {
    $name = 'yml';

} else {
    $name = $argv[1].'-yml';
    $kateg= $argv[1];
}


require_once(__DIR__ . '/../../instance.php');
require_once(__DIR__ . '/../../vendor/yml/TM_Yml.php');

$app = App::getInstance();

$getBaseHost = $app->getBaseHost();

$yml = new TM_Yml();

$yml->set_shop($app->get('TradeMaster')['yml_shop'], $app->get('TradeMaster')['yml_company'], $app->get('TradeMaster')['yml_host']);
$yml->set_delivery($app->get('TradeMaster')['yml_price']);
$yml->add_currency($app->get('TradeMaster')['yml_currency'], 1);


if ($kateg== '') {
    $katalogAll = \TradeMaster::getKatalog();
    $katalog = $katalogAll->find('idParent', 0);


}
else {
    $katalogAll =  \TradeMaster::getKatalogKat(["ih" => $kateg]);

    $katalog = $katalogAll->find('link', $kateg);


}


/* обработка категорий */




$nameTovar = '';

foreach ($katalog as $row) {

    $address = "https://" . $app->getBaseHost() . "/catalog/";
    $nameTovar = $row["nameZvena"];
    $fullLinkStrukt = $row["fullLink"];
    $yml->add_category($row["nameZvena"], (int)$row["idZvena"], (int)($row["idParent"] ? $row["idParent"] : -1));

    /* обработка товаров */
    $list = \TradeMaster::getItemList(["ih" => $row["link"], "do" => 1000, 'allTovar' => false]);


    if (count($list) > 0) {
        $tovarLink = '';
        $opisanie = '';



        foreach ($list as $item) {

            if ($item["price"] > 0) {

                $addressItem = $address  . $fullLinkStrukt . $item["link"];
                // Описание товара
                $tovar = array(

                    "price" => $item["price"],
                    "currencyId" => $app->get('TradeMaster')['yml_currency'],
                    "categoryId" => $item["vStrukture"],
                    "delivery" => 'true',
                    "store" => 'false',
                    "pickup" => 'true',
                );

                if (trim($item["ind1"])) {
                    $tovar["name"] = trim($item["name"]) . ' (' . strip_tags(Str::unEscape(urldecode($item["ind1"]))) . ')';
                    $tovar["url"] = $addressItem . '?color=' . str_replace(' ', '%20', strip_tags(Str::unEscape(urldecode($item["ind1"]))));
                } else {
                    $tovar["name"] = trim($item["name"]);
                    $tovar["url"] = $addressItem;
                }


                if (trim($item["foto_small"])) {
                    $tovar["picture"] = $app->get('TradeMaster')['yml_host'] . '/cache/' . str_replace(' ', '%20', trim($item["foto_small"]));
                }

                if (trim($item["opisanie"])) {
                    $tovar["description"] = strip_tags(str_replace('</div>',' </div>',urldecode($item["opisanie"])));
                    $opisanie = $tovar["description"];
                } else if ($opisanie) {
                    $tovar["description"] = $opisanie;
                }

                if (trim($item["strana"])) {
                    $tovar["country_of_origin"] = trim($item["strana"]);
                }
                if (trim($item["proizv"])) {
                    $tovar["vendor"] = trim($item["proizv"]);
                }
                if (trim($item["artikul"])) {
                    $tovar["vendorCode"] = trim($item["artikul"]);
                }
                $yml->add_offer($item["idTovar"], $tovar, true);

                $tovarLink = $item["link"];
            }
        }

    }

    //обработка второго уровня каталога
    $katalogSub = $katalogAll->find('idParent', $row["idZvena"]);
    if (count($katalogSub) > 0) {
        foreach ($katalogSub as $rowSub) {

            $fullLinkStrukt = $rowSub["fullLink"];
            $nameTovarSub = lcfirst($rowSub["nameZvena"]);

            $yml->add_category($rowSub["nameZvena"], (int)$rowSub["idZvena"], (int)($rowSub["idParent"] ? $rowSub["idParent"] : -1));

            $list = \TradeMaster::getItemList(["ih" => $rowSub["link"], "do" => 1000, 'allTovar' => false]);
            if (count($list) > 0) {
                $tovarLink = '';
                foreach ($list as $item) {
                    if ($item["price"] > 0) {

                        $addressItem = $address . $fullLinkStrukt . $item["link"];

                        // Описание товара
                        $tovar = array(

                            "price" => $item["price"],
                            "currencyId" => $app->get('TradeMaster')['yml_currency'],
                            "categoryId" => $item["vStrukture"],
                            "delivery" => 'true',

                        );

                        if (trim($item["ind1"])) {

                            $tovar["name"] = trim($item["name"]) . ' (' . strip_tags(Str::unEscape(urldecode($item["ind1"]))) . ')';
                            $tovar["url"] = $addressItem . '?color=' . str_replace(' ', '%20', strip_tags(Str::unEscape(urldecode($item["ind1"]))));
                        } else {
                            $tovar["name"] = trim($item["name"]);
                            $tovar["url"] = $addressItem;


                        }


                        if (trim($item["foto_small"])) {
                            $tovar["picture"] = $app->get('TradeMaster')['yml_host'] . '/cache/' . str_replace(' ', '%20', trim($item["foto_small"]));
                        }


                        if (trim($item["opisanie"])) {
                            $tovar["description"] = strip_tags(Str::unEscape(urldecode($item["opisanie"])));
                            $opisanie = $tovar["description"];
                        } else if ($opisanie) {
                            $tovar["description"] = $opisanie;
                        }


                        if (trim($item["strana"])) {
                            $tovar["country_of_origin"] = trim($item["strana"]);
                        }
                        if (trim($item["proizv"])) {
                            $tovar["vendor"] = trim($item["proizv"]);
                        }
                        if (trim($item["artikul"])) {
                            $tovar["vendorCode"] = trim($item["artikul"]);
                        }

                        if (trim($item["artikul"])) {
                            $tovar["vendorCode"] = trim($item["artikul"]);
                        }


                        $yml->add_offer($item["idTovar"], $tovar, true);


                        $tovarLink = $item["link"];
                    }
                }
            }



            //обработка третьего уровня каталога

            $katalogSubSub = $katalogAll->find('idParent', $rowSub["idZvena"]);
            if (count($katalogSubSub) > 0) {

                foreach ($katalogSubSub as $rowSubSub) {


                    $fullLinkStrukt = $rowSubSub["fullLink"];
                    $nameTovarSubSub = $nameTovarSub . " " . \TradeMaster::firstLow($rowSubSub["nameZvena"]);
                    $yml->add_category($rowSubSub["nameZvena"], (int)$rowSubSub["idZvena"], (int)($rowSubSub["idParent"] ? $rowSubSub["idParent"] : -1));


                    $list = \TradeMaster::getItemList(["ih" => $rowSubSub["link"], "do" => 1000, 'allTovar' => false]);
                    if (count($list) > 0) {
                        $tovarLink = '';
                        foreach ($list as $item) {
                            if ($item["price"] > 0) {

                                $addressItem = $address . $fullLinkStrukt . $item["link"];

                                // Описание товара
                                $tovar = array(

                                    "price" => $item["price"],
                                    "currencyId" => $app->get('TradeMaster')['yml_currency'],
                                    "categoryId" => $item["vStrukture"],
                                    "delivery" => 'true',

                                );


                                if (trim($item["ind1"])) {

                                    $tovar["name"] = trim($item["name"]) . ' (' . strip_tags(Str::unEscape(urldecode($item["ind1"]))) . ')';
                                    $tovar["url"] = $addressItem . '?color=' . str_replace(' ', '%20', strip_tags(Str::unEscape(urldecode($item["ind1"]))));
                                } else {
                                    $tovar["name"] = trim($item["name"]);
                                    $tovar["url"] = $addressItem;
                                }

                                if (trim($item["foto_small"])) {
                                    $tovar["picture"] = $app->get('TradeMaster')['yml_host'] . '/cache/' . str_replace(' ', '%20', trim($item["foto_small"]));
                                }


                                if (trim($item["opisanie"])) {
                                    $tovar["description"] = strip_tags(Str::unEscape(urldecode($item["opisanie"])));
                                    $opisanie = $tovar["description"];
                                } else if ($opisanie) {
                                    $tovar["description"] = $opisanie;
                                }


                                if (trim($item["strana"])) {
                                    $tovar["country_of_origin"] = trim($item["strana"]);
                                }
                                if (trim($item["proizv"])) {
                                    $tovar["vendor"] = trim($item["proizv"]);
                                }
                                if (trim($item["artikul"])) {
                                    $tovar["vendorCode"] = trim($item["artikul"]);
                                }
                                $yml->add_offer($item["idTovar"], $tovar, true);

                                $tovarLink = $item["link"];
                            }
                        }
                    }
                }
            }
        }
    }


}

// Пишем
if ($kateg == '') {

    file_put_contents(__DIR__ . "/../../www/" . $name . ".xml", $yml->get_xml());
} else {

    file_put_contents(__DIR__ . "/../../www/kateg-yml/" . $name . ".xml", $yml->get_xml());
}





