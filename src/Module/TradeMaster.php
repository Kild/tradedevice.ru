<?

use AEngine\Orchid\App;
use AEngine\Orchid\Module;
use AEngine\Orchid\View;

class TradeMaster extends Module
{
    public static $config = [];

    public static function initialize(App $app)
    {
        $default = [
            'version'    => 1,
            'host'       => '',
            'key'        => '',
            'cache'      => true,
            'cache_tag'  => 'trademaster',
            'cache_time' => 3600,
            'domain'     => 'trademaster.pro',
            'folder'     => 'demo',
            'paginator'  => 30,
            'currency'   => 'RUB',
            'lastView'   => 10,
            'struct'     => 0,
        ];
        static::$config = array_merge($default, $app->get('TradeMaster', []));
    }

    /**
     * Получение списка категорий
     *
     * @param array $data
     *
     * @return \AEngine\Orchid\Collection
     *
     *
     *  idZvena, idParent, nameZvena, foto, poryadok, opisanie, link, ind1, ind2, ind3, changeDate
     */
    public static function getKatalog(array $data = [])
    {
        $default = [
            'tip' => 0,
        ];
        $result = static::request([
            'path'   => 'catalog',
            'params' => array_merge($default, $data),
        ]);

        return new \AEngine\Orchid\Collection(json_decode($result, true));


    }


    /**
     * Получение списка категорий
     *
     * @param array $data
     *
     * @return \AEngine\Orchid\Collection
     *
     * idZvena, idParent, nameZvena, foto, poryadok, opisanie, link, ind1, ind2, ind3, changeDate
     *
     */
    public static function getKatalogIh(array $data = [])
    {
        $default = [
            'tip' => 0,
            'ih'  => '',
        ];

        $result = static::request([
            'path'   => 'catalogIh',
            'params' => array_merge($default, $data),
        ]);

        return new \AEngine\Orchid\Collection(json_decode($result, true));


    }

    /**
     * Получение списка категорий
     *
     * @param array $data
     *
     * @return \AEngine\Orchid\Collection
     *
     * idZvena, idParent, nameZvena, foto, poryadok, opisanie, link, ind1, ind2, ind3, changeDate
     *
     */
    public static function getKatalogKat(array $data = [])
    {
        $default = [
            'tip' => 0,
            'ih'  => '',
        ];

        $result = static::request([
            'path'   => 'catalogKat',
            'params' => array_merge($default, $data),
        ]);

        return new \AEngine\Orchid\Collection(json_decode($result, true));


    }

    /**
     * Получение всех товаров
     *
     * @param array $data
     *
     * @return array
     *
     *
     * idTovar, name, opisanie, opisanieDop,  artikul,  edIzmer,
     * vStrukture,  price,  poryadok, foto_small,
     * link, opt_price,  kolvo,
     * strihKod,   ind1,  ind2,   ind3,   ind4,   ind5,   tags,   ves,   proizv,   strana,  changeDate
     */
    public static function getItemList(array $data = [])
    {
        $default = [
            'allTovar' => false,
            'ih'       => '',
            'proizv'   => '',
            'ot'       => 0,
            'do'       => 1000,
            'tip'      => 0,
        ];
        $result = static::request([
            'path'   => 'item/list',
            'params' => array_merge($default, $data),
        ]);

        return json_decode($result, true);
    }

    /**
     * Получение товаров в группе
     *
     * @param array $data
     *
     * @return array
     */
    public static function getItemGruppa(array $data = [])
    {
        $default = [
            'gruppa' => '',
            'proizv' => '',
            'ot'     => 0,
            'do'     => 30,
        ];
        $result = static::request([
            'path'   => 'item/gruppa',
            'params' => array_merge($default, $data),
        ]);

        return json_decode($result, true);
    }

    /**
     * Получение количества товаров
     *
     * @param array $data
     *
     * @return array
     */
    public static function getItemCount(array $data = [])
    {
        $default = [
            'allTovar' => true,
            'ih'       => '',
            'proizv'   => '',
            'tip'      => 0,
        ];
        $result = static::request([
            'path'   => 'item/count',
            'params' => array_merge($default, $data),
        ]);

        return json_decode($result, true);
    }

    /**
     * Получение товара по ID
     *
     * @param array $data
     *
     * @return array
     */
    public static function getItemById(array $data = [])
    {
        $default = [
            'tovar' => 0,
            'tip'   => 0,
        ];
        $result = static::request([
            'path'   => 'item/id',
            'params' => array_merge($default, $data),
        ]);

        return json_decode($result, true);
    }

    /**
     * Создание корзины
     *
     * @param array $data
     *
     * @return array
     */
    public static function orderCart(array $data = [])
    {
        $default = [
            'tovarJson'      => '',
            'sklad'          => static::$config['storage'],
            'urlico'         => static::$config['legal'],
            'ds'             => static::$config['checkout'],
            'kontragent'     => static::$config['contractor'],
            'shema'          => static::$config['scheme'],
            'komment'        => '',
            'nameKontakt'    => '',
            'adresKontakt'   => '',
            'telefonKontakt' => '',
            'other1Kontakt'  => '',
            'dateDost'       => '',
            'valuta'         => static::$config['currency'],
            'userID'         => static::$config['tm_userID'],
        ];

        $result = static::request([
            'path'   => 'order/cart',
            'params' => array_merge($default, $data),
            'cached' => false,
            'method' => "POST",
        ]);

        return json_decode($result, true);
    }


    /**
     * Отправка  корзины
     *
     * @param array $data
     *
     * @return array
     */

    public static function orderMail(array $data = [])
    {
        $default = [
            "nomerZakaza"        => '',
            "your_name_input"    => '',
            "your_address_input" => '',
            "your_phone_input"   => '',
            "your_email_input"   => '',
            "your_date_input"    => '',
            "your_comment_input" => '',
            "items"              => '',
            "filename"           => '',
            "filedata"           => '',

        ];

        $data = array_merge($default, $data);

        require_once "../vendor/phpmailer/PHPMailerAutoload.php";

        /* Отправка письма */
        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->isHTML(true);
        $mail->Host = static::$config['mail_host'];
        $mail->Port = static::$config['mail_port'];
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = static::$config['mail_ssl'];
        $mail->Username = static::$config['mail_from'];
        $mail->Password = static::$config['mail_pass'];
        $mail->From = static::$config['mail_from'];
        $mail->FromName = static::$config['mail_name'];
        $mail->CharSet = static::$config['mail_charset'];
        $mail->Subject = str_replace("{cart_num}", $data["nomerZakaza"], static::$config['mail_subject']);

        if (!empty($data['filename'])) {
            if (isset($data['filename']) && isset($data['filedata'])) {
                $ot = strpos($data["filedata"], ",") + 1;
                $b64file = trim(substr($data["filedata"], $ot));
                $b64file = str_replace(' ', '+', $b64file);
                $decodPdf = base64_decode($b64file);
                file_put_contents($data["filename"], $decodPdf);
                $mail->addAttachment($data["filename"]);
            }
        }
        $mail->SMTPDebug = 2;


        // Мыло автора корзины
        if ($data["your_email_input"] && filter_var($data["your_email_input"], FILTER_VALIDATE_EMAIL)) {
            $mail->addAddress($data["your_email_input"], !empty($data["your_name_input"]) ? $data["your_name_input"] : "Покупатель");
        }

        // Мыло из конфига
        if (filter_var(static::$config['mail_user'], FILTER_VALIDATE_EMAIL)) {
            $mail->addBCC(static::$config['mail_user'], "Администратор");
        }

        // Тело письма
        $mail->Body = View::fetch(App::getInstance()->path('view:Email/Template.php'),
            [
                "billNumber" => $data["nomerZakaza"],
                "name"       => $data["your_name_input"],
                // "address" => $data["your_address_input"],
                "phone"      => $data["your_phone_input"],
                "email"      => $data["your_email_input"],
                "date"       => $data["your_date_input"],
                "comment"    => $data["your_comment_input"],
                //"compani" => $data["your_company_input"],
                //"companiINN" => $data["your_inn_input"],
                "items"      => $data["items"],
            ]);

        return !$mail->send() ? false : $data["nomerZakaza"];


//	            исползуется в руспай для отправки смс
//				$phone = '';
//				$id = '';
//				$key = '';
//				@file_get_contents('http://bytehand.com:3800/send?id=' . $id . '&key=' . $key . '&to=' . $phone . '&from=ruspie&text=' . urlencode('Postupil noviy zakaz #' . $result["nomerZakaza"] . ' Tel: ' . substr($data["your_phone_input"], 0, 15)));

    }


    /**
     * Получение суммы по заказу
     *
     * @param array $data
     *
     * @return array
     */
    public static function orderSumma(array $data = [])
    {
        $default = [
            'nomerZakaza' => 0,
        ];
        $result = static::request([
            'path'   => 'order/summa',
            'params' => array_merge($default, $data),
            'cached' => false,
        ]);

        return json_decode($result, true);
    }

    /**
     * Передача данных по оплате заказа
     *
     * @param array $data
     *
     * @return array
     */
    public static function orderOplata(array $data = [])
    {
        return false; // подумать
        $default = [
            'nomerZakaza' => 0,
        ];
        $result = static::request([
            'path'   => 'order/oplata',
            'params' => array_merge($default, $data),
            'cached' => false,
        ]);

        return json_decode($result, true);
    }

    /**
     * Работа с API
     *
     * @param array $data
     *
     * @return string
     */
    protected static function request(array $data = [])
    {
        $default = [
            'path'   => '',
            'params' => [],
            'cached' => true,
            'method' => "GET",
        ];
        $data = array_merge($default, $data);
        $path = implode('/', [static::$config['host'], 'v' . static::$config['version'], $data['path']]);

        if ($data['method'] == "GET") {
            // подставка ключа в параметры запроса
            $data['params']['apikey'] = static::$config['key'];
            $params = http_build_query($data['params']);
            $path .= '?' . $params;

            // если включено кеширование
            if ($data['cached'] && static::$config['cache'] === true) {
                // подмена префикса кеширования
                $prevPrefix = Mem::$prefix;
                Mem::$prefix = 'TradeMaster:' . static::$config['key'];

                // чтение из кеша
                $result = Mem::get($data['path'] . $params, false);
                //           pre('$result '.$result);

                if ($result === false) {

                    $result = file_get_contents($path);

//                    pre('path '.$data['path'] . $params);
//                    pre('$result '. $result);
//                    pre('cache_time '.static::$config['cache_time']);
//                    pre('cache_tag '.static::$config['cache_tag']);
//                    pre('!!!!');

                    // запись в кеш
                    Mem::set($data['path'] . $params, $result, static::$config['cache_time'], static::$config['cache_tag']);


                    // pre('Mem::get '.Mem::get($data['path'] . $params, false));
                }


                // возвращение предыдущего префикса
                Mem::$prefix = $prevPrefix;
            } else {
                // просто запрос
                $result = file_get_contents($path);

            }
        } else {
            // подставка ключа в параметры запроса
            $path .= '?' . http_build_query(['apikey' => static::$config['key'],]);
            $result = file_get_contents($path, false, stream_context_create([
                'http' =>
                    [
                        'method'  => 'POST',
                        'header'  => 'Content-type: application/x-www-form-urlencoded',
                        'content' => http_build_query($data['params']),
                        'timeout' => 60,
                    ],
            ]));
        }

        return $result;
    }

    /**
     * Функция строит ссылку на фото
     * Если фото есть в локальном кеше, то берётся от туда
     * Если нету, то указывается удалённая ссылка и делается попытка загрузки фото в локальный кеш
     *
     * @param array $data
     *
     * @return string
     * @throws \AEngine\Orchid\Exception\IOException
     */
    public static function getPhoto(array $data = [])
    {
        $default = [
            'file'     => '',
            'fallback' => 'https://krovlya-saiding.ru/asset/image/icon/noimage.jpeg',
        ];
        $data = array_merge($default, $data);
        $app = App::getInstance();

        if (trim($data['file'])) {
            $file = $app->pathToUrl('cache:' . trim($data['file']));

            if ($file === false) {
                $from = 'https://' . static::$config['domain'] . '/tradeMasterImages/' . static::$config['folder'] . '/' . str_replace(' ', '%20', trim($data['file']));
                $to = $app->path('cache:') . '/' . str_replace(' ', '%20', trim($data['file']));

                try {

                    $image = file_get_contents($from);
                    file_put_contents($to, $image);
                    $file = $app->pathToUrl('cache:' . str_replace(' ', '%20', trim($data['file'])));


                } catch (Exception $e) {
                    throw new \AEngine\Orchid\Exception\IOException("Неудалось скопировать файл в локальный кеш ". $data['file']);
                }

            }

           /* if ($file === false) {
                $from = 'https://' . static::$config['domain'] . '/tradeMasterImages/' . static::$config['folder'] . '/' . trim($data['file']);
                $to = $app->path('cache:') . '/' . $data['file'];

                try {

                    if (!copy($from, $to)) {

                        return $from;
                        //throw new \AEngine\Orchid\Exception\IOException("Неудалось скопировать файл в локальный кеш ". $data['file']);
                    }

                    $file = $app->pathToUrl('cache:' . trim($data['file']));
                } catch (Exception $e) {
                    // throw new \AEngine\Orchid\Exception\IOException("Неудалось скопировать файл в локальный кеш ". $data['file']);
                }

            }*/

            return str_replace('www/', '', $file);
        }

        return $data['fallback'];
    }

    /**
     * @param $data
     * @return array
     */
    public static function delimiter($data)
    {
        $arr = [];
        $data = explode(",", str_replace(" ", "", $data));
        foreach ($data as $key) {
            $key = explode(":", $key);
            if (count($key) == 2) {
                $arr[$key[0]] = $key[1];
            }
        }

        return $arr;
    }

    /**
     * @param $string
     * @return string
     */
    public static function firstLow($string)
    {
        $first = mb_substr($string, 0, 1, 'UTF-8');//первая буква
        $last = mb_substr($string, 1);//все кроме первой буквы

        return mb_strtolower($first, 'UTF-8') . $last;
    }
}
