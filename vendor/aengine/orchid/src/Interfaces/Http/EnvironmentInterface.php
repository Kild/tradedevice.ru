<?php

namespace AEngine\Orchid\Interfaces\Http;

/**
 * Environment Interface
 */
interface EnvironmentInterface
{
    public static function mock(array $settings = []);
}
