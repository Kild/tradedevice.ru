Orchid Misc
====
Functional add-ons.

#### Requirements
* Orchid Framework
* PHP >= 7.0

#### Installation
Run the following command in the root directory of your web project:
  
> `composer require aengine/orchid-misc`

#### Contributing
Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

#### License
The Orchid Framework is licensed under the MIT license. See [License File](LICENSE.md) for more information.

#### Description

##### Asset
It generates a map of resources and creates templates for single-page-application

##### Crypta
For working with encrypted string

##### Form
To easily create site forms

##### Session
Manage user sessions

##### Str
Necessary functions for string manipulation, such as the conversion of Arabic numbers in Roman

##### i18n
Simple module for the internationalization
