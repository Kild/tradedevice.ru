# Orchid-Database changelog

The latest version of this file can be found at the master branch of the
Orchid-Database repository.

## 1.0.4 (2018-03-18)
- Add methods `select`, `selectOne` and `affect`
- Add parameter `pool_name`

### 1.0.3 (2017-12-23)
- Add trim query
- Update composer file

### 1.0.2 (2017-12-02)
- Update dependency

### 1.0.1 (2017-06-24)
- Minor fixes

### 1.0.0 (2016-11-30)

- Code transferred from the Orchid project
- Add composer support
- Db now is global available class with static methods
