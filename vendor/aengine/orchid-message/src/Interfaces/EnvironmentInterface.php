<?php

namespace AEngine\Orchid\Message\Interfaces;

/**
 * Environment Interface
 */
interface EnvironmentInterface
{
    public static function mock(array $settings = []);
}
