var files = [];

$(function () {

    $('#menu').removeClass('nojs');

    $('#menuButton').bind('click', function () {
        if ($('#menu').is(':visible')) {

            $('#menu').fadeOut(500);
        }
        else {

            $('#menu').fadeIn(500);

        }


    });


    $('#searchButton').bind('click', function () {
        if ($('#searchForm').is(':visible')) {

            $('#searchForm').fadeOut(500);
        }
        else {

            $('#searchForm').fadeIn(500);

        }


    });



    $('#menuButtonClose').bind('click', function () {
        if ($('#menu').is(':visible')) {

            $('#menu').fadeOut(500);
        }
        else {

            $('#menu').fadeIn(500);
        }


    });

    // $('#searchButton').bind('click', function () {
    //
    //     $('.ya-site-form__input-text').focus();
    //
    //
    // });

    $('.goZayavka').bind('click', function () {

        window.location = '/zayavka';

    });





    if (screen.width<760){

        $(".top").sticky({topSpacing: 0});
    }
    else {
        $(".header-navigation").sticky({topSpacing: 0});


    }

    if ($('.label_better').length) {

        $(".label_better").label_better({
            easing: "bounce",
            offset: 15
        });

    }


    $(".fotorama img").click(function () {

        loc = $(this).attr('title');
        //alert(loc);

        window.location = loc;

    });




    $('.hasTooltip').each(function () { // Notice the .each() loop, discussed below



        if (screen.width<760){

            $xOffset = 0
        }
        else {

            $xOffset = -350

        }


            $(this).qtip({
            content: {
                text: $(this).next('div') // Use the "div" element next to this for the content
            },
            position: {
                adjust: {
                    x: $xOffset,
                    y: 10
                }

            }
        });
    });


    $("#attachment").change(function (event) {
        $.each(event.target.files, function (index, file) {
            var reader = new FileReader();
            reader.onload = function (event) {
                object = {};
                object.filename = file.name;
                object.data = event.target.result;
                files.push(object);
            };
            reader.readAsDataURL(file);
        });

        var file = document.getElementById('attachment').value;

        file = file.replace(/\\/g, '/').split('/').pop();

        document.getElementById('file-name').innerHTML = 'файл: ' + file;
    });



    if(window.location.pathname == '/') {

        $('.mainImg').parallax({
            naturalWidth: 300,
            naturalHeight: 200
        });



    }

});


function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
}

function deleteCookie(name) {
    setCookie(name, "", {
        expires: -1
    })
}
