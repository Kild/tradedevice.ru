/**
 * Функция возвращает окончание для множественного числа слова на основании числа и массива окончаний
 * @param  iNumber Integer Число на основе которого нужно сформировать окончание
 * @param  aEndings Array Массив слов или окончаний для чисел (1, 4, 5),
 *         например ['яблоко', 'яблока', 'яблок']
 * @return String
 */
function eos(iNumber, aEndings) {
    var sEnding, i;
    iNumber = iNumber % 100;
    if (iNumber >= 11 && iNumber <= 19) {
        sEnding = aEndings[2];
    } else {
        i = iNumber % 10;
        switch (i) {
            case (1):
                sEnding = aEndings[0];
                break;
            case (2):
            case (3):
            case (4):
                sEnding = aEndings[1];
                break;
            default:
                sEnding = aEndings[2];
        }
    }
    return sEnding;
}

Object.equals = function (x, y) {
    if (x === y) {
        return true;
    }
    // if both x and y are null or undefined and exactly the same

    if (!(x instanceof Object) || !(y instanceof Object)) {
        return false;
    }
    // if they are not strictly equal, they both need to be Objects

    if (x.constructor !== y.constructor) {
        return false;
    }
    // they must have the exact same prototype chain, the closest we can do is
    // test there constructor.

    for (var p in x) {
        if (!x.hasOwnProperty(p)) {
            continue;
        }
        // other properties were tested using x.constructor === y.constructor

        if (!y.hasOwnProperty(p)) {
            return false;
        }
        // allows to compare x[ p ] and y[ p ] when set to undefined

        if (x[p] === y[p]) {
            continue;
        }
        // if they have the same strict value or identity then they are equal

        if (typeof(x[p]) !== "object") {
            return false;
        }
        // Numbers, Strings, Functions, Booleans must be strictly equal

        if (!Object.equals(x[p], y[p])) {
            return false;
        }
        // Objects and Arrays must be tested recursively
    }

    for (p in y) {
        if (y.hasOwnProperty(p) && !x.hasOwnProperty(p)) {
            return false;
        }
        // allows x[ p ] to be set to undefined
    }
    return true;
};


simpleCart({
    checkout: {
        type: "SendForm",
        url: "/zayavka",
        extra_data: {},

    },
    cartColumns: [
        {view: "image", attr: "thumb", label: ""},
        {
            view: function (item) {
                return "<a href='" + item.get('pagelink') + "' class='btn btn-primary'>" + item.get('name') + "</a>";
            }, attr: 'pagelink', label: "Наименование товара"
        },
        {
            view: function (item) {
                return '<span>' + item.price() + '</span>';
            }, attr: "price", label: "Цена", view: 'currency'
        },
        {view: "input", attr: "quantity", label: "Колич."},
        {
            view: function (item) {
                return '<span>' + item.get('edizm') + '</span>';
            }, attr: "edizm", label: ""
        },
        {attr: "total", label: 'Всего', view: 'currency'},
        {view: "remove", text: '<i class="fa fa-times" aria-hidden="true"></i>', label: ''}

    ],
    shippingFlatRate: 0,
    cartStyle: "div"
});

simpleCart.bind('beforeCheckout', function (data) {
    var check = true,
        phone = $('#your_phone_input'),
        name = $('#your_name_input'),
        email = $('#your_email_input'),
        comment = $('#your_comment_input'),
        recaptcha = $('#g-recaptcha-response');


    if (!name.val()) {
        check = false;
        name.addClass('caution').one('change', function () {
            name.removeClass('caution');
        });
    }
    if (!phone.val()) {
        check = false;
        phone.addClass('caution').one('change', function () {
            phone.removeClass('caution');
        });
    }

    if (check) {

        data["your_name_input"] = name.val();
        data["your_phone_input"] = phone.val();
        data["your_email_input"] = email.val();
        data["your_comment_input"] = comment.val();
        data["g-recaptcha-response"] = recaptcha.val();

        $.each(files, function (index, file) {

            data["filename"] = file.filename;
            data["filedata"] = file.data;


        });


        // data["g-file"] = $('input[type=file]')[0].files[0];

        console.log(data);
        $('#p_prldr').show();

    } else {
        $("html, body").animate({scrollTop: $('.key-value').offset().top - 20}, "slow");
        throw new Error('Были заполнены не все поля в форме.');
    }
});


simpleCart.bind('update', function () {
    var $cartCount = $('[data-cartcount]'),
        $cart = $('[data-cart]'),
        $checkout = $('[data-checkout]'),
        $cartSumma = $('[data-cartsumma]');


    if (simpleCart.quantity()) {


        $('.simpleCartResult').show();

        $cartSumma.attr('data-cartsumma', simpleCart.quantity() + ' ед.');

        $cart.html('<b>' + (simpleCart.quantity()) + '</b> ' + eos((simpleCart.quantity()), ['единица', 'единицы', 'единиц']) + ' товара <br/>на сумму <b>' + simpleCart.toCurrency(simpleCart.total()) + '</b>');

        $cartSumma.removeClass('hidden');


    } else {

        $('.simpleCartResult').hide();

        $cartSumma.attr('data-cartsumma', '');

        $cartSumma.addClass('hidden');


    }
});

simpleCart.currency({
    code: "RU",
    symbol: " ₽",
    name: "Рубли",
    delimiter: " ",
    decimal: ".",
    after: true,
    accuracy: 0
});


function CallPrint() {
    window.print();
};

