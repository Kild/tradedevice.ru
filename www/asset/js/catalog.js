$((undefined) => {
    let path = {};

    $(window).bind('statechange', function (e) {
        e.preventDefault();

        let state = History.getState();

       // $(document).scrollTop(0);

        $.post(state.url).done((page) => {
            let $page = $('<div>').html(page);

            $('#p_prldr').hide();
            $('.breadcrumbs').html($page.find('.breadcrumbs').html()).find('#catalog-category-select').attr('onchange', '');
            $('div.section').html($page.find('div.section').html());

            if ($('.sp-wrap').length) {

                $(document.body)
                    .off('click', '.sp-thumbs')
                    .off('mouseover')
                    .off('touchstart')
                    .off('click', '.sp-tb-active a')
                    .off('mouseenter', '.sp-non-touch .sp-large')
                    .off('mouseleave', '.sp-non-touch .sp-large')
                    .off('click', '.sp-non-touch .sp-zoom')
                    .off('click', '.sp-large a')
                    .off('click', '#sp-next')
                    .off('click', '#sp-prev')
                    .off('click', '.sp-lightbox');

                $('.sp-wrap').smoothproducts();

                $('#descriptionTabs').responsiveTabs({
                    startCollapsed: 'tabs'
                });
            }


            if ($('.sp-wrap2').length) {


                $('.sp-wrap2').smoothproducts();


            }





            $('body,html').animate({scrollTop: path[location.pathname] || 0}, 600);
        }).fail(() => {
            location.reload();
        });
    });

    $('body')
        .on('click', 'a[href^="/catalog"]', (e) => {
            if (e.target.baseURI != e.target.href) {
                $('#p_prldr').show();
            }

            e.preventDefault();

            let $link = $(e.currentTarget),
                text = $link.text();

            path[location.pathname] = $(document).scrollTop();

            if (!text) {
                text = $link.parents('li').find('h3').text();
            }
            History.pushState(null, text, $link.attr('href'));
        })
        .on('change', '#catalog-category-select', (e) => {
            e.preventDefault();

            let $select = $(e.currentTarget),
                $options = $select.find('option'),
                path = location.href;

            $options.each(function() {
                path = path.replace($(this).val(), '');
            });

            History.pushState(null, $select.find(':selected').text(), path + $select.val());
        })
        .on('click', '.sp-thumbs a', (e) => {
            e.preventDefault();

            $('.itemTov').hide();
            $('.itemTov.brand').show();

            $('#productOffer' + $(e.currentTarget).data('id')).show();
        })
        .on('click', '#toList', (e) => {
            e.preventDefault();

            $(".katalogItems").addClass("list");

            $("#toList").addClass("cur");
            $("#toVitr").removeClass("cur");

            setCookie("viewTovar", "toList", 36000);
        })
        .on('click', '#toVitr', (e) => {
            e.preventDefault();

            $(".katalogItems").removeClass("list");

            $("#toList").removeClass("cur");
            $("#toVitr").addClass("cur");

            setCookie("viewTovar", "toVitr", 36000);
        })
        .on('click', '.item_add', (e) => {
            e.preventDefault();

            id = e.target.dataset.for;

            topY = $("a[data-id='" + id + "']").offset().top;
            leftX = $("a[data-id='" + id + "']").offset().left;

            if (screen.width<760){


                leftcart =  $("#cartIcon").offset()['left'];
                topcart =   $("#cartIcon").offset()['top'];

            }
            else {

                leftcart =  $("#cartMenu").offset()['left'];
                topcart =   $("#cartMenu").offset()['top'];

            }


            $("a[data-id='" + id + "']")
                .clone()
                .css({
                    'position': 'absolute',
                    'z-index': '11100',
                    top: topY,
                    left: leftX,
                    'display': 'block',
                    'width': '50px',
                    'height': '50px',
                    'padding': '5px',
                    'background-color': '#fff',
                    'background-size': 'cover',
                    'background-position': 'center'
                })
                .appendTo("body")
                .animate({
                    opacity: 0.55,
                    left: leftcart,
                    top: topcart
                }, 800, function () {
                    $(this).remove();
                });
        });

    $('.sp-wrap').smoothproducts();



    $('#descriptionTabs').responsiveTabs({
        startCollapsed: 'tabs'
    });

    if ($('.sp-wrap2').length) {


        $('.sp-wrap2').smoothproducts();


    }
});

