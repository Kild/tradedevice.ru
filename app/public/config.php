<?php

use AEngine\Orchid\Message\Request;
use AEngine\Orchid\Message\Response;
use AEngine\Orchid\Misc\Asset;
use AEngine\Orchid\View;
use Controller\Catalog;
use Controller\Main;
use Controller\News;
use Controller\NewsRecord;
use Controller\Page;
use Controller\Cart;
use Controller\Req;

// resource map
Asset::$map = [



    '/*' => [
        // less
        // '/asset/less/style.less',

        // js
        //  '/asset/js/less.min.js',
        '/asset/js/jquery-3.2.1.min.js',
        '/asset/js/jquery.magnifier.js',
        '/asset/js/simplecart.js',
        '/asset/js/trademaster.js',
        '/asset/js/scripts.js',
        '/asset/js/fotorama.js',
        '/asset/js/jquery.sticky.js',
        '/asset/js/jquery.qtip.min.js',
        '/asset/js/jquery.responsiveTabs.min.js',



        // css
         '/asset/css/style.css',
    ],


    '/' => [
        '/asset/js/parallax.min.js',
        '/asset/js/smoothproducts.min.js',

    ],


    '/zayavka' => [

        // js
        'https://www.google.com/recaptcha/api.js',
        '/asset/js/jquery.label_better.min.js',



    ],

    // js

    '/catalog' => [
        '/asset/js/smoothproducts.min.js',
        '/asset/js/jquery.history.js',
        '/asset/js/catalog.js',
    ],

    '/catalog/*' => [
        '/asset/js/smoothproducts.min.js',

        // '/asset/js/scriptCatalog.js',
        '/asset/js/jquery.history.js',
        '/asset/js/catalog.js',

    ],
];

// global template
View::$layout = $app->path('view:Layout.php');

// router obj
$router = $app->router();

/*
 * GET      - get resource
 * POST     - create resource
 * PUT      - update resource
 * DELETE   - delete resource
 */

//Bin::run('test');

// example route


$router->get('/sitemap', Req::class);
$router->get('/nocache', Req::class);
$router->get('/yml', Req::class);
$router->get('/cup', Req::class);

$router->any('/upload', Req::class);


$router->get('/', Main::class);

$router->get('/*', [Main::class, 'p404'], -20)->addMiddleware(function (Request $req, Response $res, $next) {

    if (\AEngine\Orchid\Misc\Str::end('/', $req->getUri()->getPath())) {
        /** @var Response $res */
        $res = $res->withHeader('location', rtrim($req->getUri()->getPath(), '/'));
        $res = $res->withStatus(301);

        return $res;
    }

    return $next($req, $res);
});

$router->map(['GET', 'POST'], '/catalog/*', Catalog::class)
    ->addMiddleware(function (Request $req, Response $res, $next) {
        if (\AEngine\Orchid\Misc\Str::end('/', $req->getUri()->getPath())) {
            /** @var Response $res */
            $res = $res->withHeader('location', rtrim($req->getUri()->getPath(), '/'));
            $res = $res->withStatus(301);

            return $res;
        }

        return $next($req, $res);
    });

$router->map(['GET', 'POST'], '/catalog', Catalog::class);
$router->get('/:url', Page::class, -10)
    ->addMiddleware(function (Request $req, Response $res, $next) {

        if (\AEngine\Orchid\Misc\Str::end('/', $req->getUri()->getPath())) {
            /** @var Response $res */
            $res = $res->withHeader('location', rtrim($req->getUri()->getPath(), '/'));
            $res = $res->withStatus(301);

            return $res;
        }

        return $next($req, $res);
    });


$router->any('/zayavka', Cart::class);
$router->get('/zayavka/complete', Cart::class);

$router->get('/news', News::class);
$router->get('/news/:url', NewsRecord::class)
    ->addMiddleware(function (Request $req, Response $res, $next) {

        if (\AEngine\Orchid\Misc\Str::end('/', $req->getUri()->getPath())) {
            /** @var Response $res */
            $res = $res->withHeader('location', rtrim($req->getUri()->getPath(), '/'));
            $res = $res->withStatus(301);

            return $res;
        }

        return $next($req, $res);
    });





