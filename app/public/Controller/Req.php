<?php

namespace Controller;

use AEngine\Orchid\App;
use AEngine\Orchid\Controller;
use AEngine\Orchid\Message\Request;
use AEngine\Orchid\Message\Response;
use AEngine\Orchid\View;
use Bin;




class Req extends Controller
{
    public function index(Request $request, Response $response, $args)
    {
        if ($request->getUri()->getPath() == "/sitemap"){

            Bin::run('sitemap', []);


            View::setGlobal('title', 'sitemap выполнены');
            View::setGlobal('description', 'sitemap выполнены');
        }

        if ($request->getUri()->getPath() == "/yml"){


            if (isset($_GET["kateg"])){

                Bin::run('tm_yml', ['kateg' => $_GET["kateg"] ]);
            }
            else{

                Bin::run('tm_yml');

            }



            View::setGlobal('title', 'yml выполнены');
            View::setGlobal('description', 'yml выполнены');

        }



        if ($request->getUri()->getPath() == "/nocache"){

            Bin::run('clear_cache');

            View::setGlobal('title', 'clear_cache выполнены');
            View::setGlobal('description', 'clear_cache выполнены');

        }




        if ($request->getUri()->getPath() == "/cup"){



            View::setGlobal('title', 'Центр управления полетом');
            View::setGlobal('description', 'управление генераторами файлов для разных каналов');


            $canon='https://krovlya-saiding.ru/cup';
            View::setGlobal('canon', $canon);

            return $response->write(
                new View(
                    App::getInstance()->path('view:Page/Cup.php'),
                    []
                )
            );

        }


        return Main::ok($request, $response);


    }
}