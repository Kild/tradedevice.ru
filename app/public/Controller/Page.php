<?php

namespace Controller;

use AEngine\Orchid\App;
use AEngine\Orchid\Controller;
use AEngine\Orchid\Message\Request;
use AEngine\Orchid\Message\Response;
use AEngine\Orchid\View;

class Page extends Controller
{
    public function index(Request $request, Response $response, $args)
    {


        $page = \Model\Page::fetch($args);


        if ($page->get('id')) {

            $path = $request->getUri()->getPath();

            View::setGlobal('title', $page->get('title') . '. Фронт Хаус - всё для кровли и фасада в Москве');
            View::setGlobal('canon', 'https://' . App::getInstance()->getBaseHost() . '/' . $page->get('url'));
            View::setGlobal('description', $page->get('title'));


            $template = 'view:Page/Page.php';

            if ($page->get('template') == 'page') {

                $template = 'view:Page/Page.php';
            }
            if ($page->get('template') == 'page-2') {

                $template = 'view:Page/Page2.php';
            }

            if ($page->get('template') == 'page-montaj') {

                $template = 'view:Page/Page-montaj.php';
            }





            return $response->write(
                new View(
                    App::getInstance()->path($template),
                    [
                        'model' => $page,
                        'path' => $path,
                        'katalog' => \TradeMaster::getKatalog()
                    ]
                )
            );
        }



        View::setGlobal('title', '404 - страница не найдена');
        View::setGlobal('canon', '');
        View::setGlobal('description', '404 - страница не найдена');
        return Main::p404($request, $response);


    }
}