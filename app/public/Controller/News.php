<?php

namespace Controller;

use AEngine\Orchid\App;
use AEngine\Orchid\Controller;
use AEngine\Orchid\Message\Request;
use AEngine\Orchid\Message\Response;
use AEngine\Orchid\View;

class News extends Controller
{
    public function index(Request $request, Response $response, $args)
    {
        View::setGlobal('title', 'Полезно знать.'. ' Алмазный инструмент в Уфе');
        View::setGlobal('canon', 'https://'.App::getInstance()->getBaseHost().'/news');
        View::setGlobal('description', 'Полезно знать');


        $path = $request->getUri()->getPath();
        $pathArray = explode('/', $path);


        return $response->write(
            new View(
                App::getInstance()->path('view:Page/News.php'),
                [
                    'list' => \Collection\News::fetch($args),
                    'pathArray'  => $pathArray,
                ]
            )
        );



    }
}
