<?php

namespace Controller;

use AEngine\Orchid\App;
use AEngine\Orchid\Controller;
use AEngine\Orchid\Message\Request;
use AEngine\Orchid\Message\Response;
use AEngine\Orchid\Misc\Str;
use AEngine\Orchid\View;


class Catalog extends Controller
{
    public function index(Request $request, Response $response)
    {

        $title = "Каталог кровли и фасада в Москве. Фронт Хаус.";
        $canon = App::getInstance()->getBaseHost() .'/catalog';
        $description = "Каталог кровли и фасада в Москве с доставкой и монтажем. Фронт Хаус.";

        $katalog = \TradeMaster::getKatalog();



        $path = rtrim($request->getUri()->getPath(), '/');
        $pathArray = explode('/', $path);

        $pathArrayCount = count($pathArray);

        $validPath = false;

        $data = [
            "url" => end($pathArray),
        ];

        $parentKat = -1;
        $parentKat2 = -1;



        if ($data["url"]) {

            $tekKat = $katalog->find("link", $data["url"]);



            if ($tekKat->get(0)["idParent"] == 0) {  //если родитель равен 0, то он первый уровень

                $validPath = ($pathArrayCount == 3);   //в пути первого уровеня 3 части

            } else {

                $parentKat = $katalog->find("idZvena", $tekKat->get(0)["idParent"]);  //если не первый уровень, то находим родителя


                if ($parentKat->get(0)["idParent"] == 0) {   //если у родителя родитель равен 0, то он второй уровень

                    $validPath = ($pathArrayCount == 4) && $parentKat->get(0)["link"] == $pathArray[2];

                } else {

                    $parentKat2 = $katalog->find("idZvena", $parentKat->get(0)["idParent"]);  //если не второй уровень, то находим его родителя из второго уровня


                    $validPath = ($pathArrayCount == 5) && $parentKat->get(0)["link"] == $pathArray[3] && $parentKat2->get(0)["link"] == $pathArray[2];


                }

            }


            if (($validPath && $tekKat && count($tekKat)) || $data["url"] == "catalog") // открываем страницу каталога
            {
                $idParent2 = -1;
                $linkParent2 = "";
                $nameParent2 = "";

                if ($data["url"] == "catalog") { // для /catalog
                    $idParent = 0;
                    $linkParent = "";
                    $nameParent = "Каталог";



                } else {
                    $idParent = $tekKat->get(0)["idZvena"];
                    $linkParent = $tekKat->get(0)["link"];
                    $nameParent = $tekKat->get(0)["nameZvena"];


                    if ($tekKat->get(0)["idParent"] == 0) {

                        // для /catalog/stroitelnoe-oborudovanie

                        $title = $nameParent . '  в Москве';
                        $canon .= '/' . $linkParent;
                        $description .= ' / ' . $title;

                    } else {

                        if ($parentKat->get(0)["idParent"] == 0) {   //если у родителя родитель равен 0, то он второй уровень

                            // для http://102tools.ru/catalog/stroitelnoe-oborudovanie
                            $canon .= '/' . ($parentKat2 == -1 ? '' : $parentKat2->get(0)["link"] . '/') . $parentKat->get(0)["link"] . "/" . $linkParent;
                            $description .= ' / ' . $parentKat->get(0)["nameZvena"] . ' ' . \TradeMaster::firstLow($nameParent) . '  в Москве';
                            $title = $nameParent . '  в Москве';

                            $idParent2 = $parentKat->get(0)["idZvena"];
                            $linkParent2 = $parentKat->get(0)["link"];
                            $nameParent2 = $parentKat->get(0)["nameZvena"];

                        } else {

                            // для http://102tools.ru/catalog/stroitelnoe-oborudovanie/stanki-dlya-rezki-armatury
                            $idParent2 = $parentKat->get(0)["idZvena"];
                            $linkParent2 = $parentKat->get(0)["link"];
                            $nameParent2 = $parentKat->get(0)["nameZvena"];

                            $canon .= '/' . ($idParent2 == -1 ? '' : $parentKat2->get(0)["link"] . '/') . $parentKat->get(0)["link"] . "/" . $linkParent;
                            $description .= ' / ' . $parentKat->get(0)["nameZvena"] . ' ' . \TradeMaster::firstLow($nameParent) . ' в Москве';
                            $title = $nameParent . '  в Москве';

                        }
                    }
                }





                View::setGlobal('title', $title);
                View::setGlobal('canon', $canon);
                View::setGlobal('description', $description);


                $list = \TradeMaster::getItemList(["ih" => $data["url"], "do" => 1000, 'allTovar' => false]);
                $kateg = $katalog->find("idParent", $idParent);

                // for post
                if ($request->isPost()) {





                    return $response->write(
                        View::fetch(
                            App::getInstance()->path('view:Catalog/Catalog.php'),
                                [
                                "item" => $tekKat, //выбранная категория
                                "list" => $list, //товар в выбранной категории
                                "kateg" => $kateg, // подкатегории в выбранной категории
                                'katalog' => $katalog, // вся структура товара

                                'linkParent' => $linkParent, // ссылка на род. категорию
                                'nameParent' => $nameParent, // наименование на род. категории
                                'idParent' => $idParent, // id на род. категории

                                'linkParent2' => $linkParent2, // ссылка на род.род. категорию
                                'nameParent2' => $nameParent2, // наименование на род.род. категории
                                'idParent2' => $idParent2, // id на род.род. категории

                                'path' => $path,
                                'title' => $title,
                            ]
                        )
                    );
                }


                return $response->write(
                    new View(
                        App::getInstance()->path('view:Catalog/Catalog.php'),
                        [
                            "item" => $tekKat, //выбранная категория
                            "list" => $list, //товар в выбранной категории
                            "kateg" => $kateg, // подкатегории в выбранной категории
                            'katalog' => $katalog, // вся структура товара

                            'linkParent' => $linkParent, // ссылка на род. категорию
                            'nameParent' => $nameParent, // наименование на род. категории
                            'idParent' => $idParent, // id на род. категории

                            'linkParent2' => $linkParent2, // ссылка на род.род. категорию
                            'nameParent2' => $nameParent2, // наименование на род.род. категории
                            'idParent2' => $idParent2, // id на род.род. категории

                            'path' => $path,
                            'title' => $title,
                        ]
                    )
                );

            } else {
                // открываем страницу товара
                // товар собирается из записей товаров с одинаковой ссылкой



                $list = \TradeMaster::getItemList(["ih" => $pathArray[count($pathArray) - 2], "do" => 1000, 'allTovar' => false]);  // находим весь товар в родительской категории
                $listC = new \AEngine\Orchid\Collection($list);
                $listC = $listC->find("link", $data["url"]); //  фильтруем только с текущей сылкой

                $idParent2 = -1;
                $linkParent2 = "";
                $nameParent2 = "";




                if (count($listC)) {

                    $idZvena = $listC->get(0)["vStrukture"];
                    $tekKat = $katalog->find("idZvena", $idZvena); //  выбранная категория
                    $linkZvena = $tekKat->get(0)["link"];
                    $nameZvena = $tekKat->get(0)["nameZvena"];


                    $title =   $listC->get(0)["name"]. ' купить в Москве'. ($_GET['color'] ?? null ? '. Цвет '. $_GET['color'] : ''). (($listC->get(0)["price"] != 0) ? ('. Цена '.number_format($listC->get(0)["price"], 0, '.', ' ')). ' р. за '.(( $listC->get(0)["edIzmer"] != "") ?  $listC->get(0)["edIzmer"]  : 'шт') : '');


                    if ($tekKat->get(0)["idParent"] == 0) {


                        $canon .= '/' . $linkZvena . '/' . $data["url"];

                        $validPath = ($pathArrayCount == 4) && $tekKat->get(0)["link"] == $pathArray[2];


                    } else {

                        $parentKat = $katalog->find("idZvena", $tekKat->get(0)["idParent"]);

                        if ($parentKat->get(0)["idParent"] == 0) {   //если у родителя родитель равен 0, то он второй уровень


                            $validPath = ($pathArrayCount == 5) && $tekKat->get(0)["link"] == $pathArray[3] && $parentKat->get(0)["link"] == $pathArray[2];

                        } else {

                            $idParent2 = $parentKat->get(0)["idZvena"];
                            $parentKat2 = $katalog->find("idZvena", $parentKat->get(0)["idParent"]);  //если не второй уровень, то находим его родителя из второго уровня
                            $validPath = ($pathArrayCount == 6) && $tekKat->get(0)["link"] == $pathArray[4] && $parentKat->get(0)["link"] == $pathArray[3] && $parentKat2->get(0)["link"] == $pathArray[2];


                        }

                        $canon .= '/' . ($idParent2 == -1 ? '' : $parentKat2->get(0)["link"] . '/') . $parentKat->get(0)["link"] . "/" . $linkZvena. "/" .$listC->get(0)["link"];


                    }

                   $description = strip_tags(str_replace('</div>',' </div>',urldecode($listC->get(0)["opisanie"])));

                    if (!$description){


                        $description= $title .' купить в Москве';
                    }

                   // . ' за '.(( $listC->get(0)["edIzmer"] != "") ?  $listC->get(0)["edIzmer"]  : 'шт')

                    if ($validPath) {


                        View::setGlobal('title', $title);
                        View::setGlobal('canon', $canon);
                        View::setGlobal('description', $description);


                        // for post
                        if ($request->isPost()) {
                            return $response->write(
                                View::fetch(
                                    App::getInstance()->path('view:Catalog/Item.php'),
                                    [
                                        'item' => $tekKat, //выбранная категория
                                        "list" => $listC, //выбранные товары
                                        'katalog' => $katalog, // вся структура
                                        'linkZvena' => $linkZvena, // ссылка на род. категорию
                                        'nameZvena' => $nameZvena, // наименование на род. категории
                                        'path' => $path, // путь
                                        'idZvena' => $idZvena, // id выбранной категории
                                        'title' => $title,
                                    ]
                                )
                            );
                        }

                        return $response->write(
                            new View(
                                App::getInstance()->path('view:Catalog/Item.php'),
                                [
                                    'item' => $tekKat, //выбранная категория
                                    "list" => $listC, //выбранные товары
                                    'katalog' => $katalog, // вся структура
                                    'linkZvena' => $linkZvena, // ссылка на род. категорию
                                    'nameZvena' => $nameZvena, // наименование на род. категории
                                    'path' => $path, // путь
                                    'idZvena' => $idZvena, // id выбранной категории
                                    'title' => $title,
                                ]
                            )
                        );
                    }

                }


            }

        }

        View::setGlobal('title', '404 - страница не найдена. Фронт Хаус.');
        View::setGlobal('canon', '');
        View::setGlobal('description', '404 - страница не найдена');
        return Main::p404($request, $response);
    }
}