<?php

namespace Controller;

use AEngine\Orchid\App;
use AEngine\Orchid\Controller;
use AEngine\Orchid\Message\Request;
use AEngine\Orchid\Message\Response;
use AEngine\Orchid\View;

class Main extends Controller
{
    public function index(Request $request, Response $response)
    {
        View::setGlobal('title', 'Трейддевайс');
        View::setGlobal('canon', App::getInstance()->getBaseHost());
        View::setGlobal('description', 'Трейддевайс');

        // pre($request );

        $linkZvena = "";
        $nameZvena = "Каталог";

        return $response->write(
            new View(
                App::getInstance()->path('view:Page/Main.php'),
                [
                    'catalog'   => \TradeMaster::getKatalog(),
                    'linkZvena' => $linkZvena,
                    'nameZvena' => $nameZvena,
                ]
            )
        );
    }

    public static function p404(Request $request, Response $response)
    {
        $response = $response->write(
            new View(
                App::getInstance()->path('view:Page/404.php')
            )

        );
        $response = $response->withStatus(404);

        return $response;
    }

    public static function ok(Request $request, Response $response)
    {

        $canon='';
        View::setGlobal('canon', $canon);


        $response = $response->write(
            new View(
                App::getInstance()->path('view:Page/Ok.php')
            )

        );
        $response = $response->withStatus(404);

        return $response;
    }


}
