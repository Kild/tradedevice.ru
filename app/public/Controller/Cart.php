<?php

namespace Controller;

use AEngine\Orchid\App;
use AEngine\Orchid\Controller;
use AEngine\Orchid\Message\Request;
use AEngine\Orchid\Message\Response;
use AEngine\Orchid\Misc\Form;
use AEngine\Orchid\View;

class Cart extends Controller
{

    public static function getCurlData($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
        $curlData = curl_exec($curl);
        curl_close($curl);
        return $curlData;
    }

    public function index(Request $request, Response $response)
    {
        View::setGlobal('title', 'Заявка');
        View::setGlobal('canon', 'https://' . App::getInstance()->getBaseHost() . '/zayavka');
        View::setGlobal('description', 'Заявка магазина кровли и фасада в Москве');


        if ($request->isPost()) {


            $default = [
                "itemCount" => 0,
                "filename"=> '',
                "filedata"=> '',
            ];
            $data = array_merge($default, $request->getParams());

            $google_url = 'https://www.google.com/recaptcha/api/siteverify';
            $secret = '6LeCKEYUAAAAAIs5Ejbc0NckYoC_fhePa-AaqlG_';
            $ip = $_SERVER['REMOTE_ADDR'];
            $recaptchaResponse = $data["g-recaptcha-response"];
            $url = $google_url . "?secret=" . $secret . "&response=" . $recaptchaResponse . "&remoteip=" . $ip;

            $res = Cart::getCurlData($url);
            $res = json_decode($res, true);

            if ($res['success']) {

                // Продолжаем проверку данных формы


                if ($data["itemCount"]) {

                    $items = [];

                    /* Реорганизуем массив с товарами в корзине */
                    for ($i = 1; $i <= $data["itemCount"]; $i++) {
                        $items[$i]["name"] = $data["item_name_" . $i];
                        $items[$i]["quantity"] = $data["item_quantity_" . $i];
                        $items[$i]["price"] = $data["item_price_" . $i];
                        $items[$i]["options"] = \TradeMaster::delimiter($data["item_options_" . $i]);
                    }

                    if (!array_key_exists('your_date_input', $data)) {
                        $data["your_date_input"] = date("d.m.Y H:i");
                    }

                    $dataTMAPI = [
                        'tovarJson' => json_encode($items, JSON_UNESCAPED_UNICODE),
                        'komment' => $data['your_comment_input'],
                        'nameKontakt' => $data['your_name_input'],
                        //'adresKontakt' => iconv( "cp1251", "utf-8", $data['your_address_input']),
                        'telefonKontakt' => $data['your_phone_input'],
                        'other1Kontakt' => $data['your_email_input'],
                        'dateDost' => $data['your_date_input'],
                    ];

                   

                    $result = \TradeMaster::orderCart($dataTMAPI);


//                    if ($result["nomerZakaza"]) {

                    $dataMail = [
                        'nomerZakaza'        => '',
                        "your_name_input"    => $data['your_name_input'],
                        "your_phone_input"   => $data['your_phone_input'],
                        "your_email_input"   => $data['your_email_input'],
                        "your_comment_input" => $data['your_comment_input'],
                        "items"              => $items,
                        "filename"           => $data['filename'],
                        "filedata"           => $data['filedata'],

                    ];


                    $mailResult = \TradeMaster::orderMail($dataMail);
                    if (isset($data['filename'])) {
                        if (is_file($data['filename'])) {
                            unlink($data['filename']);
                        }
                    }

                    View::setGlobal('title', 'Заказ принят!');

                    return $response->write(
                        new View(
                            App::getInstance()->path('view:Page/CartComplete.php'),
                            [
                                'nomerZakaza' => '',
                            ]
                        )
                    );
//                    }


                } else {


                    $dataMail = [
                        'nomerZakaza'        => '',
                        "your_name_input"    => $data['your_name_input'],
                        "your_phone_input"   => $data['your_phone_input'],
                        "your_email_input"   => $data['your_email_input'],
                        "your_comment_input" => $data['your_comment_input'],
                        "items"              => [],
                        "filename"           => $data['filename'],
                        "filedata"           => $data['filedata'],


                    ];


                    $mailResult = \TradeMaster::orderMail($dataMail);

                    if (isset($data['filename'])) {
                        if (is_file($data['filename'])) {
                            unlink($data['filename']);
                        }
                    }

                    View::setGlobal('title', 'Заказ принят!');

                    return $response->write(
                        new View(
                            App::getInstance()->path('view:Page/CartComplete.php'),
                            [
                                'nomerZakaza' => '',
                            ]
                        )
                    );


                }

            } else {

                return $response->write(
                    new View(
                        App::getInstance()->path('view:Page/Cart.php'),
                        [
                            "your_name_input"    => $data['your_name_input'],
                            "your_phone_input"   => $data['your_phone_input'],
                            "your_email_input"   => $data['your_email_input'],
                            "your_comment_input" => $data['your_comment_input'],
                        ]
                    )
                );

            }

        }


        return $response->write(
            new View(
                App::getInstance()->path('view:Page/Cart.php'),
                [
                    "your_name_input"    => '',
                    "your_phone_input"   => '',
                    "your_email_input"   => '',
                    "your_comment_input" => '',
                    'catalog' => \TradeMaster::getKatalog(),
                ]
            )
        );


    }
}
