<?php

namespace Controller;

use AEngine\Orchid\App;
use AEngine\Orchid\Controller;
use AEngine\Orchid\Message\Request;
use AEngine\Orchid\Message\Response;
use AEngine\Orchid\View;

class NewsRecord extends Controller
{
    public function index(Request $request, Response $response, $args)
    {


        $path = $request->getUri()->getPath();
        $pathArray = explode('/', $path);

        $newsRecord = \Model\News::fetch($args);

        if ($newsRecord->get('id')){
            View::setGlobal('title', 'Полезно знать про ' . mb_strtolower($newsRecord->get('title')). '. Алмазный инструмент в Уфе');
            View::setGlobal('canon', 'https://' . App::getInstance()->getBaseHost() . '/news/' . $newsRecord->get('url'));
            View::setGlobal('description', 'Полезно знать про ' . mb_strtolower($newsRecord->get('title')));

            return $response->write(
                new View(
                    App::getInstance()->path('view:Page/NewsRecord.php'),
                    [
                        'model' => $newsRecord,
                        'pathArray' => $pathArray,
                    ]
                )
            );


        }

        View::setGlobal('title', '404 - страница не найдена');
        View::setGlobal('canon', '');
        View::setGlobal('description', '404 - страница не найдена');
        return Main::p404($request, $response);



    }
}
