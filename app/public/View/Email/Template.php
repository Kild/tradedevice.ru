<?php

/**
 * @var $billNumber
 * @var $name
 * @var $address
 * @var $date
 * @var $phone
 * @var $email
 * @var $comment
 * @var $items
 * @var $compani
 * @var $companiINN
 */
?>

<div class="b-letter__body">
    <div class="b-letter__body__calendars"></div>
    <div class="js-body b-letter__body__wrap">
        <div class="js-helper js-readmsg-msg">
            <style type="text/css"></style>
            <div>
                <div id="style_15068708580000000917_BODY">
                    <div class="class_1512764792">
                        <table bgcolor="#EBEDEF" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                            <tr>
                                <td style="padding:15px 5px;" align="center" valign="middle">
                                    <span style="font-size: 10pt;font-family: Arial;color: #939aa4;">Благодарим за оформление заявки в интернет-магазине!</span>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:0px 5px 15px;" align="center" valign="top">
                                    <table bgcolor="#FFFFFF" cellpadding="0" cellspacing="0" width="600">
                                        <tbody>
                                        <tr>
                                            <td style="border-bottom: 1px solid #EBEDEF;padding: 15px;" align="center" valign="middle">
                                                <table bgcolor="#FFFFFF" cellpadding="0" cellspacing="0" width="100%">
                                                    <tbody>
                                                    <tr>

                                                        <td align="left" valign="middle" width="60%">
                                                            <span style="font-size: 10pt;font-family: Arial;color: #303030;">krovlya-saiding.ru  - современный интернет-магазин материалов для кровли и фасада в Москве.</span>
                                                        </td>
                                                        <td align="right" valign="middle">
                                                            <span style="font-size: 16pt;font-family: Arial;color: #303030;">+7 (495) 199-4235</span>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 15px;line-height:1.5em;border-bottom: 1px solid #EBEDEF;" align="left" valign="top">
                                                <span style="font-size: 11pt;font-family: Arial;color: #303030;">
                                                    <b>Уважаемый(ая) <?= $name; ?>,</b> <br> <br>
                                                    Ваша заявка  <b><?= (!empty($billNumber) ? 'номер ' + $billNumber : ""); ?></b>  принята. <br><br>

                                                    <b>Заявка:</b><br>
                                                    Телефон: <?= $phone; ?><br>
                                                    E-Mail: <?= $email; ?><br>
                                                    Комментарий: <?= $comment; ?><br>
                                                     <br>

                                                    <?php

                                                    if (count($items) > 0) {
                                                        ?>

                                                        <b>Состав заявки:</b>
                                                        <ol style="margin: 0px; line-height: 1.8;">
                                                    <?php
                                                    $summ = 0;
                                                    foreach ($items as $key) {
                                                        $item_summ = $key["price"] * $key["quantity"];
                                                        $summ += $item_summ;
                                                        ?>
                                                        <li>
                                                              <?= $key["name"]; ?> - <?= $key["quantity"] . ' ' . $key["options"]["edizm"]; ?><br>
                                                              Цена:<?= $key["price"]; ?> ₽. Сумма:<b><?= $item_summ; ?> ₽</b></li>
                                                        <?php
                                                    }
                                                    ?>
                                                        </ol>
                                                        <br>
                                                        Сумма к оплате: <b><?= $summ; ?> ₽</b>
                                                        <br>

                                                        <?php
                                                    }
                                                    ?>
                                                    <br>
                                                    Для подтверждения заявки наш менеджер свяжется с Вами в ближайшее время.<br>
                                                    Заявки, оформленные после 19:00 подтверждаются на следующий рабочий день. <br>

                                                    <br>
                                                    Получить информацию по Вашей заявке, аннулировать или перенести заявку Вы можете в рабочее время по телефону:
                                                    +7 (495) 199-4235.
                                                    <br>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 15px 15px 25px;line-height:1.5em;" align="left" valign="top">
                                                <span style="font-size: 11pt;font-family: Arial;color: #303030;"> С уважением,<br>krovlya-saiding.ru<br>
                                                    <span>+7 (495) 199-4235</span><br>
                                                    <a href="mailto:info@krovlya-saiding.ru" style="color: #5BC0E9;" target="_blank" rel=" noopener noreferrer">info@krovlya-saiding.ru</a><br>
                                                    <a href="https://krovlya-saiding.ru" traget="_blank" style="color: #5BC0E9;" target="_blank" rel=" noopener noreferrer">www.krovlya-saiding.ru</a><br>
                                                </span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
