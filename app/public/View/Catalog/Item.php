<?

use AEngine\Orchid\App;
use AEngine\Orchid\View;

/**
 * @var \AEngine\Orchid\Collection $item , //выбранная категория
 * @var \AEngine\Orchid\Collection $list ,  //выбранные товары
 * @var $linkZvena // ссылка на род. категорию
 * @var $nameZvena // наименование на род. категории
 * @var $katalog , // вся структура
 * @var $idZvena // id выбранной категории
 * @var $path // путь
 * @var $title ,
 */


$app = App::getInstance();

$link = '/catalog';
//$title = 'Каталог продукции';
$proizv = '';
$primen = '';

$desc1 = $desc2 = '';
$data = 0;
$fotoZvena = '';
$pathArray = explode('/', $path);



echo View::fetch(App::getInstance()->path('view:Element/Nav.php'),
    [
        'path' => $pathArray,
        'menu' => $katalog,
    ]);



echo(View::fetch($app->path('view:/Element/ProductCategoryComboItem.php'), [
    'item' => $list, //выбранная категория
    'menu' => $katalog, // вся структура
    'linkParent' => $linkZvena, // ссылка на род. категорию
    'nameParent' => $nameZvena,  // наименование на род. категории
    'idParent' => $idZvena, // id выбранной категории
    'path' => $path, // путь
]));




?>


</div> <!--end header-->

<div class="content item">
    <div class="section">

        <?= View::fetch($app->path('view:/Element/ProductListItem.php'), [
            'list' => $list,
            'idZvena' => $idZvena,
            'path' => $path,
            'title' => $title,
        ]); ?>
    </div> <!--end section-->
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $(() => {
            $("[itemprop]").removeAttr("itemprop");
        });
    });
</script>


