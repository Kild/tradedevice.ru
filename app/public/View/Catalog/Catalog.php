<?

use AEngine\Orchid\App;
use AEngine\Orchid\Misc\Str;
use AEngine\Orchid\View;

/**
 * @var \AEngine\Orchid\Collection $item
 * @var \AEngine\Orchid\Collection $list
 * @var \AEngine\Orchid\Collection $kateg
 * @var \AEngine\Orchid\Collection $katalog
 * @var                            $linkParent
 * @var                            $nameParent
 * @var                            $idParent
 * @var                            $path
 * @var                            $title
 *
 * @var                            $linkParent2 ,
 * @var                            $nameParent2 ,
 * @var                            $idParent2
 *
 */


$app = App::getInstance();

$link = '/catalog';
//$title = 'Каталог ';
$proizv = '';
$primen = '';
$idZvena = 0;
$desc1 = $desc2 = '';
$data = 0;
$fotoZvena = '';


if (count($item)) {
    // $title = $item[0]['nameZvena'];
    $desc1 = $item[0]['opisanie'];
    $fotoZvena = $item[0]['foto'];
    $idZvena = $item[0]['idZvena'];
}

$fotoZvena = \TradeMaster::getPhoto(['file' => $fotoZvena]);

$pathArray = explode('/', $path);


echo View::fetch(App::getInstance()->path('view:Element/Nav.php'),
    [
        'path' => $pathArray,
        'menu' => $katalog,
    ]);


echo View::fetch($app->path('view:/Element/ProductCategoryCombo.php'), [
    'item' => $item, //выбранная категория
    'menu' => $katalog,
    'linkZvena' => $linkParent,
    'nameZvena' => $nameParent,
    'idZvena' => $idZvena,
    'path' => $path,
]);
?>

</div> <!--end header-->

<div class="content item">


    <div class="section">
        <div class="section-top">
            <div class="section-info">
                <h1><?= $nameParent; ?></h1>

            </div> <!--end section-info-->
        </div> <!--end section-top-->

        <?= View::fetch($app->path('view:/Element/CategList.php'), [
            'katalog' => $katalog,
            'podkateg' => $kateg,
            'linkParent' => $linkParent,
            'nameParent' => $nameParent,
            'linkParent2' => $linkParent2, // ссылка на род.род. категорию
            'nameParent2' => $nameParent2, // наименование на род.род. категории
            'width' => 250,
        ]); ?>


        <?
        if (isset($_COOKIE["viewTovar"])) {

            $viewTovar = $_COOKIE["viewTovar"];

        } else {

            $viewTovar = "toVitr";
        }

        ?>
        <?= View::fetch($app->path('view:/Element/ProductList.php'), [
            'list' => $list,
            'idZvena' => $idZvena,
            'path' => $path,
            'viewTovar' => $viewTovar,
        ]); ?>

        <?
        if (!!trim($desc1)) { ?>

            <div class="section-mid">
                <div class="section-info">

                    <p><?= Str::unEscape(urldecode($desc1)); ?></p>

                </div> <!--end section-info-->
            </div> <!--end section-top-->

        <? }


        if ((count($kateg) + count($list)) == 0) {


            ?>

            <div class="vRazrabotke">
                <h3>Извините, пожалуйста, но данный раздел интернет-магазина находится на стадии наполнения товаром</h3>
                <p>В ближайшее время здесь появится товар</p>
                <h3>Позвоните к нам или закажите обратный звонок</h3>
                <p>и мы проконсултируем по выбору нужных Вам материалов уже сейчас</p>
            </div>

            <?
        }


        ?>


    </div> <!--end section-->
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $(() => {
            $("[itemprop]").removeAttr("itemprop");
        });
    });
</script>
