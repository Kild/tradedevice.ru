<?
/**
 * @var array $path
 * @var array $menu
 */


?>

<div class="menu nojs" id="menu">

    <ul class="header-navigation">
        <li class="navLogo"><a href="/"></a></li>
        <li <?= in_array("catalog", $path) ? ' class="navMenu menu__item--current"': ' class="navMenu"';  ?>><a href="/catalog">Каталог</a>



            <!--noindex-->
           <!-- <ul class="menuHover" id="menuHover">

                <?/*



                foreach ($menu as $index => $value) {

                    if ($value["idParent"] == 0) {

                        */?>
                        <li<?/*= in_array($value['link'], $path) ? ' class="currentKateg"' : ""; */?>><a <?/*= in_array($value['link'], $path) ? ' class="currentKategA"' : ""; */?> href="/catalog/<?/*= $value['link'] */?>"><?/*= $value["nameZvena"] */?></a>
                            <?/*
                            $podKats = $menu->find("idParent", $value["idZvena"]);
                            if (count($podKats)) {
                                */?>
                                <ul class="podKats">
                                    <?/*
                                    foreach ($podKats as $index1 => $value1) {
                                        */?>
                                        <li <?/*= in_array($value1['link'], $path) ? ' class="currentKateg"' : ""; */?>>
                                            <a <?/*= in_array($value1['link'], $path) ? ' class="currentKategA"' : ""; */?> href="/catalog/<?/*= $value['link'] . '/' . $value1['link'] */?>"><?/*= $value1["nameZvena"] */?></a>

                                            <?/*
                                            $podKats2 = $menu->find("idParent", $value1["idZvena"]);
                                            if (count($podKats2)) {
                                                */?>
                                                <ul class="podKats2">
                                                    <?/*
                                                    foreach ($podKats2 as $index2 => $value2) {
                                                        */?>
                                                        <li <?/*= in_array($value2['link'], $path) ? ' class="currentKateg"' : ""; */?>>
                                                            <a <?/*= in_array($value2['link'], $path) ? ' class="currentKategA"' : ""; */?> href="/catalog/<?/*= $value['link'] . '/' .$value1['link'] . '/' . $value2['link'] */?>"><?/*= $value2["nameZvena"] */?></a>
                                                        </li>
                                                        <?/*
                                                    }
                                                    */?>
                                                </ul>
                                                <?/*
                                            }
                                            */?>
                                        </li>
                                        <?/*
                                    }
                                    */?>
                                </ul>
                                <?/*
                            }
                            */?>
                        </li>
                        <?/*
                    }
                }
                */?>
            </ul>-->
            <!--/noindex-->

        </li>
        <li data-cartsumma="" id="cartMenu" <?= in_array("zayavka", $path) ? ' class="menu__item--current hidden"': 'class="hidden"'  ?>><a href="/zayavka">Сделать заказ</a></li>

        <li <?= in_array("delivery", $path) ? ' class="menu__item--current"': "";  ?>><a href="/delivery">Доставка и оплата</a></li>
        <li <?= in_array("montaj-saidinga-i-krovli", $path) ? ' class="menu__item--current"': "";  ?>><a href="/montaj-saidinga-i-krovli">Монтаж</a></li>
        <li <?= in_array("contacts", $path) ? ' class="menu__item--current"': "";  ?>><a href="/contacts">Контакты</a></li>

    </ul>
    <i class="fa fa-times" aria-hidden="true" id="menuButtonClose"></i>

</div> <!--end header-menu-->
<!---->
<!--<script>-->
<!---->
<!---->
<!--    $('.navMenu').hover(-->
<!--        function(){-->
<!--            $('.menuHover').show();-->
<!--        }-->
<!--        );-->
<!---->
<!---->
<!--    $('#menuHover').mouseleave(-->
<!--        function(){-->
<!--            $('#menuHover').hide();-->
<!--        }-->
<!--    );-->
<!---->
<!---->
<!--</script>-->


