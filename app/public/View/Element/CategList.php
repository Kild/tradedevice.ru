<?
/**
 *
 * @var \AEngine\Orchid\Collection $katalog ,
 * @var \AEngine\Orchid\Collection $podkateg ,
 * @var                            $linkParent ,
 * @var                            $nameParent ,
 * @var                            $width ,
 * @var                            $linkParent2 ,
 * @var                            $nameParent2 ,
 */
?>




<? if (count($podkateg)) { ?>
    <div class="section-bottom">
        <ul class="catalog">
            <? foreach ($podkateg as $index => $value) {
                $fotoZvena = \TradeMaster::getPhoto(['file' => $value['foto']]);

                $podkateg = $value["link"];

                ?>


                <li>
                    <a href='/catalog/<?= (!!$linkParent2 ? $linkParent2 . '/' : '') . (!!$linkParent ? $linkParent . '/' : '') . $value["link"] ?>'>
                        <div style="background-image: url('<?= $fotoZvena ?>')">
                        </div>
                    </a>
                    <a href='/catalog/<?= (!!$linkParent2 ? $linkParent2 . '/' : '') . (!!$linkParent ? $linkParent . '/' : '') . $value["link"] ?>'><h3><?= $value["nameZvena"] ?></h3></a>

                    <?

                    $kateg = $katalog->find("idParent", $value["idZvena"]);
                    ?>

                    <ul class="podkateg">
                        <?
                        foreach ($kateg as $index => $value) {
                            $kategPodkateg = $katalog->find("idParent", $value["idZvena"]);
                            $podkategkateg = $value["link"];

                            ?>
                            <li>
                                <a href='/catalog/<?= (!!$linkParent2 ? $linkParent2 . '/' : '') . (!!$linkParent ? $linkParent . '/' : '') . $podkateg . '/' . $value["link"] ?>'>
                                    <?= $value["nameZvena"] ?>
                                </a>

                              <? if (count($kategPodkateg)>0) { ?>
                                    <ul class="podkategkateg">
                                        <?
                                        foreach ($kategPodkateg as $index2 => $value2) {
                                            $kategPodkateg = $katalog->find("idParent", $value2["idZvena"]);

                                            ?>
                                            <li><a href='/catalog/<?= (!!$linkParent2 ? $linkParent2 . '/' : '') . (!!$linkParent ? $linkParent . '/' : '') . $podkateg . '/' . $podkategkateg . '/' . $value2["link"] ?>'>
                                                    <?= $value2["nameZvena"] ?></a>
                                            </li>
                                            <?
                                        }
                                        ?>
                                    </ul>

                                <? } ?>

                            </li>
                        <? }
                        ?>

                    </ul>


                </li>
            <? } ?>
        </ul>
    </div><!--end section-bottom-->
<? } ?>
