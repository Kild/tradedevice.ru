<?


/**
 * use AEngine\Orchid\Collection;
 * @var \AEngine\Orchid\Collection $list
 * @var   $idZvena
 * @var   $path
 * @var  $title
 */

use AEngine\Orchid\App;
use AEngine\Orchid\Misc\Str;
use AEngine\Orchid\View;


if (count($list)) {

    ?>
    <div class="item-page" itemscope itemtype="http://schema.org/Product">
        <?
        $color = '';
        $tovarLink = '';
        $foto1 = '';
        $fotoList = [];
        $fotoListId = [];
        $fotoIdListNext = [];

        $fotoListDom = [];

        $nameTov = '';
        $edIzmerTov = '';
        $opisanie = '';
        $opisanieDop = '';



        $list->sort(function ($a, $b, $args) {
            if ($a['link'] == $b['link']) {
                if (in_array($args, [$a['ind1'], $b['ind1']])) {
                    if ($args == $a['ind1']) {
                        return -1;
                    } else {
                        return 1;
                    }
                }
                if ($a['price'] == $b['price']) {
                    return (int)$a['poryadok'] - (int)$b['poryadok'];
                }
                return (float)$a['price'] - (float)$b['price'];
            }
            return strcmp($a['link'], $b['link']);
        }, $_GET['color'] ?? null);



        foreach ($list as $index => $item) {

            if ($nameTov == '' && $item["name"] != '') {
                $nameTov = $item["name"];
            }

            if ($edIzmerTov == '' && $item["edIzmer"] != '') {
                $edIzmerTov = $item["edIzmer"];
            }
            if ($opisanie == '' && $item["opisanie"] != '') {
                $opisanie = $item["opisanie"];
            }
            if ($opisanieDop == '' && $item["opisanieDop"] != '') {
                $opisanieDop = $item["opisanieDop"];
            }
        }



        foreach ($list as $index => $item) {

        if ($tovarLink != $item["link"]) {

        ?><h1 itemprop="name"><?= $nameTov ?></h1>
        <div class="item-offers">
            <div class="itemKatalogRowInput">
                <div class="itemTov brand">
                    <p>Производитель: </p>
                    <img src="/asset/image/brands/<?= $item["proizv"]; ?>.jpg">
                </div>

                <?
                }

                $fotoListNext = explode(';', trim($item["foto_small"]));
                $foto1 = \TradeMaster::getPhoto(['file' => str_replace(' ', '%20', $fotoListNext[0])]);

                $tmp = [];
                $tmp['file'] = $fotoListNext[0];
                $tmp['id'] = $item["idTovar"];
                $tmp['price'] = $item["price"];
                $fotoList[] = $tmp;

                $tmp = [];
                $fl = 0;
                foreach ($fotoListNext as $row) {

                    if ($fl) {
                        $tmp['file'] = $row;
                    }
                    $fl = $fl + 1;

                    if ($tmp) {
                        $fotoListDom[] = $tmp;
                    }
                }





                ?>

                <div class="itemTov simpleCart_shelfItem " itemprop="offers" itemscope itemtype="http://schema.org/Offer" <?= ($tovarLink == $item["link"]) ? 'style="display: none;"' : '' ?>
                     id="productOffer<?= $item["idTovar"]; ?>">

                    <p>Цвет: </p>
                    <h2><?= $item["ind1"]; ?></h2>
                    <div class="itemTovKolvo">
                        <div>
                            <?= ($item["price"] != 0 ? '<p>Цена: </p>' : '<p></p>') ?>
                            <div class="itemKatalogRowParam itemKatalogRowParamPrice">
                                <?= ($item["price"] != 0 ? number_format($item["price"], 0, '.', ' ') . ' р. за ' . $item['edIzmer'] : 'Уточнить цену') ?>
                            </div>
                        </div>

                        <div>
                            <p>Количество: </p>
                            <div class="itemKatalogRowParam itemKatalogRowParamPrice quant">
                                <input class="item_quantity inp" type="text" size="5" value="1">
                                <?= (($item["edIzmer"] != "") ? '<span class="item_edizm" >' . $item["edIzmer"] . '</span>' : '<span class="item_edizm" >шт.</span>')


                                ?>
                            </div>
                        </div>
                    </div>

                    <input data-for="<?= $item["idTovar"]; ?>" class="item_add inpAdd" type="button" value="Добавить в заказ" data-id="<?= $item["idTovar"] ?>" data-text="В заказе">
                    <img class="item_thumb" style="display: none;" itemprop="image" src="<?= $foto1 ?>" alt="<?= $item["name"] ?>">
                    <span class="item_price" style="display: none;"><?= number_format($item["price"], 2, '.', '') ?></span>
                    <span class="item_id" style="display: none;"><?= $item["idTovar"]; ?></span>
                    <span class="item_name" style="display: none;"><?= $item["name"] . ' ' . $item["ind1"] . ' ' . $item["ind2"] . ' ' . $item["ind3"]; ?></span>
                    <span class="item_artikul" style="display: none;"><?= $item["artikul"]; ?></span>
                    <span class="item_pagelink" style="display: none;"><?= $item["ind1"] ? $path . '?color=' . str_replace(' ', '%20', strip_tags(Str::unEscape(urldecode($item["ind1"])))) : $path ?></span>
                    <meta itemprop="price" content="<?= number_format($item["price"], 2, '.', '') ?>">
                    <meta itemprop="priceCurrency" content="RUB">
                    <link itemprop="availability" href="http://schema.org/InStock">
                </div>
                <?

                $tovarLink = $item["link"];
                }
                ?>


            </div>
        </div>


        <div class="sp-wrap">
            <div class="sp-loading"><img src="/asset/image/spinning-circles.svg" alt="" width="50" height="50"></div>
            <?
            $i = 0;
            foreach ($fotoList as $row) {
                $foto = \TradeMaster::getPhoto(['file' => str_replace(' ', '%20', $row["file"])]);
                ?>
                <a href="<?= $foto ?>"><img src="<?= $foto ?>" alt='<?= $nameTov ?>' data-id="<?= $row["id"] ?>" <?= $i == 0 ? '' : 'style = "display: none"' ?> ><span><?= $item["price"] ?></span></a>
                <?

                $i = $i + 1;
            }
            ?>
        </div>

        <div id="descriptionTabs">
            <ul>

                <li><a href="#tab-1">Описание</a></li>

                <?


                if (strlen(trim($opisanieDop)) > 0) { ?>

                    <li><a href="#tab-2">Характеристики</a></li>
                    <?
                };

                if (count($fotoListDom) > 0) { ?>

                    <li><a href="#tab-3">Примеры применения</a></li>
                    <?
                };
                ?>


            </ul>


            <div id="tab-1" itemprop="description"><?= Str::unEscape(urldecode($opisanie)) ?></div>
            <?

            if (strlen(trim($opisanieDop)) > 0) {

                $table = Str::unEscape(urldecode($opisanieDop));
                $table = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $table);
                $table = preg_replace(
                    array(
                        // Remove invisible content
                        '@<head[^>]*?>.*?</head>@siu',
                        '@<style[^>]*?>.*?</style>@siu',
                        '@<script[^>]*?.*?</script>@siu',
                        '@<object[^>]*?.*?</object>@siu',
                        '@<embed[^>]*?.*?</embed>@siu',
                        '@<applet[^>]*?.*?</applet>@siu',
                        '@<noframes[^>]*?.*?</noframes>@siu',
                        '@<noscript[^>]*?.*?</noscript>@siu',
                        '@<noembed[^>]*?.*?</noembed>@siu',
                        // Add line breaks before and after blocks
                        '@</?((address)|(blockquote)|(center)|(del))@iu',
                        '@</?((div)|(h[1-9])|(ins)|(isindex)|(p)|(pre))@iu',
                        '@</?((dir)|(dl)|(dt)|(dd)|(li)|(menu)|(ol)|(ul))@iu',
                        '@</?((table)|(th)|(td)|(caption))@iu',
                        '@</?((form)|(button)|(fieldset)|(legend)|(input))@iu',
                        '@</?((label)|(select)|(optgroup)|(option)|(textarea))@iu',
                        '@</?((frameset)|(frame)|(iframe))@iu',
                    ),
                    array(
                        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                        "\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0",
                        "\n\$0", "\n\$0",
                    ),
                    $table);


                ?>
                <div id="tab-2"><?= $table ?></div>
                <?
            };
            if (count($fotoListDom) > 0) { ?>
                <div id="tab-3">
                    <div class="sp-wrap">
                        <div class="sp-loading"><img src="/asset/image/spinning-circles.svg" alt="" width="50" height="50"></div>
                        <?
                        foreach ($fotoListDom as $row) {
                        $fotoDom = \TradeMaster::getPhoto(['file' => str_replace(' ', '%20', $row["file"])]);
                            ?>
                        <a href="<?= $fotoDom; ?>"><img class="fotoListDom" alt='<?= $nameTov ?>' src="<?= $fotoDom; ?>" ></a>
                            <?
                        }
                        ?>
                    </div>

                </div>
                <?
            };
            ?>

        </div>

        <div class="reklama"><span>Если Вы затрудняетесь с выбором материала для ремонта или строительства, то позвоните к нам или закажите звонок, мы вместе примем решение. У нас большой опыт в сфере ремонта с применением сайдинга и кровельных материалов. Мы гарантируем индивидуальный подход в решении Вашей задачи по ремонту или строительству дома.</span></div>




    </div>


    <?

}

?>

