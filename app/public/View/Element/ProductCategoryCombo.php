<?
/**
 * @var array $item
 * @var array $menu
 * @var   $idZvena
 * @var   $linkZvena
 * @var   $nameZvena
 * @var $path
 */


$def = 1;
$idZvenaCombo = 0;
$uriStr = '';
$link = "/catalog";
$listCombo = $menu->find('idParent', $idZvena);

if (!!$item) {
    $idZvenaCombo = $item['idZvena'];
}
$uri = explode('/', $path);
$firstCount = count($uri);

if (count($listCombo) == 0) {

    unset($uri[count($uri) - 1]);
    $def = 0;
    $idZvenaCombo = $idZvena;
    $listCombo = $menu->find('idParent', $item->get(0)["idParent"]);
}
$uriStr = implode("/", $uri);

?>
<div class="breadcrumbs">
    <div class="path">
        <p><a href="/">Главная</a> / <a href="/catalog">Каталог</a> /
            <?
            if ($firstCount > 2) {
            $i = 0;
            foreach ($uri as $key => $val) {
                if ($i < 4) {
                    if (!!$val) {
                        if ($val != 'catalog') {
                            $link .= "/" . $val;
                            echo '<a href="' . $link . '" ' . (count($uri) - 1 == $key ? ' class="current"' : '') . '>' . ($menu->find("link", $val)->get(0)["nameZvena"]) . '</a>';
                            echo ' / ';
                        }
                    }
                }
                $i++;
            };

            ?>
            <label id="catalog-category-label">
                <select id="catalog-category-select" onchange="window.location = '<?= $uriStr; ?>' + this.options[this.selectedIndex].value">
                    <option <? if ($def) { ?> selected="selected" <? } ?> value="">Все категории</option>
                    <? foreach ($listCombo

                    as $index => $value) { ?>
                    <option <? if (!$def) {
                        if ($value["idZvena"] == $idZvenaCombo) { ?> selected="selected" <? }
                    } ?> value="/<?= $value["link"]; ?>">
        <p><?= $value["nameZvena"]; ?></p>
        </option>
        <?
        }
        ?>
        </select>
        </label>
        </p> <?
        }

        else { ?>

            </p>

        <? }
        ?>
    </div> <!--end header-source-->

</div>