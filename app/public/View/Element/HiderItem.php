<?

/**
 * @var  $idZvena
 */


if (($idZvena == 5) || ($idZvena == 12) || ($idZvena == 13) || ($idZvena == 14) || ($idZvena == 27) || ($idZvena == 28) || ($idZvena == 29) || ($idZvena == 30)) { ?>
    <div class="itemKatalogHRow">
        <div class="itemKatalogHCell">Диаметр, мм./ посад. отверстие, мм.</div>
        <div class="itemKatalogHCell">Толщ. слоя, мм./ толщ. корп. мм./ высота слоя мм.</div>
        <div class="itemKatalogHCell">Конструкция корпуса</div>
        <div class="itemKatalogHCell">Цена</div>
        <div class="itemKatalogHCommandCell">
            <a onclick="javascript:CallPrint();" title="Распечатать прайс-лист" class="print"></a>
            <a id="cart" href="/cart" class="cart" data-cartcount="0"></a>
        </div>
    </div>


    <?php
} elseif (($idZvena == 2) || ($idZvena == 17) || ($idZvena == 18) || ($idZvena == 34)) { ?>

    <div class="itemKatalogHRow">
        <div class="itemKatalogHCell">Диаметр, мм / Длина коронки, мм</div>
        <div class="itemKatalogHCell">Количество сигментов, шт</div>
        <div class="itemKatalogHCell">Крепление</div>
        <div class="itemKatalogHCell">Цена</div>
        <div class="itemKatalogHCommandCell">
            <a onclick="javascript:CallPrint();" title="Распечатать прайс-лист" class="print"></a>
            <a id="cart" href="/cart" class="cart" data-cartcount="0"></a>
        </div>
    </div>

    <?php
} elseif ($idZvena == 33) { ?>
    <div class="itemKatalogHRow">

        <div class="itemKatalogHCell">Диаметр, мм / Глубина сверления, мм</div>
        <div class="itemKatalogHCell">Количество сигментов, шт</div>
        <div class="itemKatalogHCell">Крепление</div>
        <div class="itemKatalogHCell">Цена</div>
        <div class="itemKatalogHCommandCell">
            <a onclick="javascript:CallPrint();" title="Распечатать прайс-лист" class="print"></a>
            <a id="cart" href="/cart" class="cart" data-cartcount="0"></a>
        </div>
    </div>
    <?php
} elseif (($idZvena == 4) || ($idZvena == 15)) { ?>
    <div class="itemKatalogHRow">
        <div class="itemKatalogHCell">Диаметр, мм</div>
        <div class="itemKatalogHCell">Длина сверла, мм</div>
        <div class="itemKatalogHCell">Количество сигментов, шт</div>
        <div class="itemKatalogHCell">Цена</div>
        <div class="itemKatalogHCommandCell">
            <a onclick="javascript:CallPrint();" title="Распечатать прайс-лист" class="print"></a>
            <a id="cart" href="/cart" class="cart" data-cartcount="0"></a>
        </div>
    </div>
    <?php
} elseif (($idZvena == 16)) { ?>

    <div class="itemKatalogHRow">
        <div class="itemKatalogHCell">Радиус сегмента, мм</div>
        <div class="itemKatalogHCell">Соответствие диаметру коронки, мм</div>
        <div class="itemKatalogHCell">Размер сегмента(длина/толщина/высота)</div>
        <div class="itemKatalogHCell">Цена</div>
        <div class="itemKatalogHCommandCell">
            <a onclick="javascript:CallPrint();" title="Распечатать прайс-лист" class="print"></a>
            <a id="cart" href="/cart" class="cart" data-cartcount="0"></a>

        </div>
    </div>

    <?php
} elseif (($idZvena == 31) || ($idZvena == 32) || ($idZvena == 22)) { ?>
    <div class="itemKatalogHRow">
        <div class="itemKatalogHCell">Диаметр, мм</div>
        <div class="itemKatalogHCell">Количество сигментов, шт</div>
        <div class="itemKatalogHCell">Посадочное отверстие, мм</div>
        <div class="itemKatalogHCell">Цена</div>
        <div class="itemKatalogHCommandCell">
            <a onclick="javascript:CallPrint();" title="Распечатать прайс-лист" class="print"></a>
            <a id="cart" href="/cart" class="cart" data-cartcount="0"></a>
        </div>
    </div>
    <?php
}


?>

