<?


/**
 * use AEngine\Orchid\Collection;
 * @var \AEngine\Orchid\Collection $list
 * @var   $idZvena
 * @var   $path
 * @var $viewTovar
 */

use AEngine\Orchid\App;
use AEngine\Orchid\Misc\Str;
use AEngine\Orchid\View;


if (count($list)) {

    $list = new \AEngine\Orchid\Collection($list);


    $list->sort(function ($a, $b) {
        if ($a['link'] == $b['link']) {
            if ($a['price'] == $b['price']) {
                return (int)$a['poryadok'] - (int)$b['poryadok'];
            }

            return (float)$a['price'] - (float)$b['price'];
        }
        return strcmp($a['link'], $b['link']);
    });

    ?>
    <div class="contrView">
        <i class="fa fa-list <?= $viewTovar != 'toVitr' ? 'cur' : '' ?>" aria-hidden="true" data-description="Списком" id="toList"></i>
        <i class="fa fa-th-large <?= $viewTovar == 'toVitr' ? 'cur' : '' ?>" aria-hidden="true" data-description="Витрина" id="toVitr"></i>
    </div>
    <div class="section-bottom">
<ul class="katalogItems  <?= $viewTovar != 'toVitr' ? 'list' : '' ?>">
    <?
    $tovarLink = '';
    $tovarPrice = -1;

    foreach ($list as $index => $item) {

        if ($tovarLink != $item["link"] && $tovarLink != '' && $tovarPrice != -1) { ?>
            </ul></div></li>
            <?
        };

        if ($tovarLink != $item["link"]) {

            ?>
            <li>
            <div class="itemKatalog">
            <a href="<?= $path . '/' . $item["link"] ?>" class="itemKatalogNameA">
                <div class="itemKatalogImg" style="background-image: url('<?= \TradeMaster::getPhoto(['file' => explode(';', trim($item["foto_small"]))[0]]) ?>')">
                </div>
                <h2><?= $item["name"] ?></h2>
                <div class="itemKatalogFirstPrice">
                    <br>
                </div>
            </a>
            <div class="brandKat" style="background-image: url('/asset/image/brands/<?= $item["proizv"]; ?>.jpg')"></div>
            <div class="itemKatalogKupit"><a href="<?= $path . '/' . $item["link"] ?>"><?= ($item["price"] != 0 ? number_format($item['price'], 0, '.', ' ') . ' р./' . $item['edIzmer'] : 'Уточнить цену') ?></a></div>
            <?
            $fotoList = explode(';', trim($item["foto_small"]));
            $foto1 = \TradeMaster::getPhoto(['file' => str_replace(' ', '%20', $fotoList[0])]);
            ?>
            <ul class="katalogItemsColor">
            <a href="<?= $path . '/' . $item["link"] . '?color=' . str_replace(' ', '%20', strip_tags(Str::unEscape(urldecode($item["ind1"])))) ?>">
                <li style="background-image:url('<?= $foto1 ?>')">
                </li>
            </a>
            <?
        } else { ?>

            <?
            $fotoList = explode(';', trim($item["foto_small"]));
            $foto1 = \TradeMaster::getPhoto(['file' => str_replace(' ', '%20', $fotoList[0])]);
            ?><a href="<?= $path . '/' . $item["link"] . '?color=' . str_replace(' ', '%20', strip_tags(Str::unEscape(urldecode($item["ind1"])))) ?>">
            <li style="background-image:url('<?= $foto1 ?>')">
            </li>
            </a>
        <? }


        $tovarLink = $item["link"];
        $tovarPrice = $item["price"];
    }

    ?>
    </div></li>
    </ul>

    </div><!--end section-bottom-->
    <?

}

?>

