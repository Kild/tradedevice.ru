<?
/**
 * @var   $catalog
 * @var   $your_name_input
 * @var   $your_phone_input
 * @var   $your_email_input
 * * @var   $your_comment_input
 */

use AEngine\Orchid\App;
use AEngine\Orchid\View;

?>

<?= View::fetch(App::getInstance()->path('view:Element/Nav.php'),
    [
        'path' => ['zayavka'],
        'menu' => $catalog,
    ]); ?>


<div class="breadcrumbs">
  <div class="path">
    <p><a href="/">Главная</a> / Заказ</p>
  </div>
</div> <!--end header-source-->
</div> <!--end header-->

<section>

  <main>
    <div class="container">

      <h1>Сделать заказ</h1>

      <div class="containerCart">

        <form id="cart-form" method="post" enctype="multipart/form-data">

          <div class="simpleCartResult">
            <div class="simpleCart_items"></div>
            <div class="simpleCart_grandTotal"></div>

          </div>

          <!--                    pre($your_name_input);-->
          <!--                    pre($your_phone_input);-->
          <!--                    pre($your_email_input);-->
          <!--                    pre($your_comment_input);-->

          <!--class="label_better" data-new-placeholder="Username"-->


          <div data-view="form" class="cart-result">


            <div class='formItem'>
                <?= \AEngine\Orchid\Misc\Form::text('nameKontakt', [
                    'id'                   => 'your_name_input',
                    'placeholder'          => 'Ваше имя*',
                    'value'                => $your_name_input,
                    'class'                => 'label_better"',
                    'data-new-placeholder' => 'Ваше имя*',

                ]) ?>
            </div>

            <div class='formItem'>
                <?= \AEngine\Orchid\Misc\Form::text('telefonKontakt', [
                    'id'                   => 'your_phone_input',
                    'placeholder'          => 'Телефон*',
                    'value'                => $your_phone_input,
                    'class'                => 'label_better"',
                    'data-new-placeholder' => 'Телефон*',

                ]) ?>
            </div>

            <div class='formItem'>
                <?= \AEngine\Orchid\Misc\Form::text('emailKontakt', [
                    'id'                   => 'your_email_input',
                    'placeholder'          => 'E-mail (отправим копию заказа)',
                    'value'                => $your_email_input,
                    'class'                => 'label_better"',
                    'data-new-placeholder' => 'E-mail (отправим копию заказа)*',

                ]) ?>
            </div>

            <div class='formItem'>
                <?= \AEngine\Orchid\Misc\Form::textarea('komment', [
                    'id'                   => 'your_comment_input',
                    'placeholder'          => 'Комментарии',
                    'value'                => $your_comment_input,
                    'class'                => 'label_better"',
                    'data-new-placeholder' => 'Комментарии',
                ]) ?>

            </div>


            <div class='formItemFile'>
              <p>Если у Вас уже есть сформированная заявка, то отправьте её нам, приложив соответствующий файл</p>

              <div class='buttonFile'>
                <label for="attachment"> <span>Приложить файл <i class="fa fa-upload"></i></span></label>
                  <input type="hidden" name="MAX_FILE_SIZE" value="10000">
                  <input class="your_file_input" name="attachment" id="attachment" type="file"  >
              </div>
              <div id="file-name"></div>
            </div>

            <div class="zakonPers formItem">Отправляя заявку, даю согласие на обработку персональных данных, указанных при оформлении заявки,в целях исполнения данной
              заявки в соответствии с <a href="/konfidencialnost-personalnoy-informacii">Условиями</a> и условиями <a href="/delivery">оплаты и доставки</a>.
            </div>

            <div class='formItem'>
                <div class="g-recaptcha" data-sitekey="6LeCKEYUAAAAALbMElqlx5qhQHs82v5CF0F2toS4"></div>
            </div>

          </div>

        </form>


        <div id="cartButton" class="cartButton">
          <button class="simpleCart_checkout">Отправить заявку</button>
        </div>


      </div>
    </div>
  </main>

</section>


