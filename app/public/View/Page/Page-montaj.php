<?
/**
 * @var Model\Page $model
 * @var          $path
 *  @var          $katalog
 */

use AEngine\Orchid\Misc\Str;
use AEngine\Orchid\View;
use AEngine\Orchid\App;

?>

<?= View::fetch(App::getInstance()->path('view:Element/Nav.php'),
    [
        'path' => explode('/', $path),
        'menu' => $katalog,
    ]); ?>

<div class="breadcrumbs">
    <div class="path">
        <p><a href="/">Главная</a> / <?= $model->get('title'); ?></p>
    </div>
</div> <!--end header-source-->
</div> <!--end header-->

</div>


<div class="first-montag">

    <div class="container">

        <h1><?= $model->get('title'); ?></h1>
        <div class="montag-offer">
            <ul>
                <li>Отделка сайдингом под ключ" в кратчайшие сроки</li>

                <li>Наши цены ниже среднерыночных на 7 - 15%</li>

                <li>Монтаж выполняем качественно и в срок</li>

                <li>Цена за работу в процессе не вырастет</li>

                <li>Выкупаем оставшийся материал</li>

                <li>Гарантия на работы 5 лет</li>
            </ul>

        </div>
    </div>
    <div class="montag-uslovia">
        <div>Без предоплаты оплата по факту выполненных работ</div>
        <div>Выезд инженера для расчета в течение 24 часов</div>
    </div>
</div>
<div class="container montag">

    <h2>Выполняем любые виды отделки дома под ключ</h2>

    <div class="vidi-montag">

        <div class="vid-montag">
            <div class="vid-name-montag">Отделка сайдингом</div>
            <div class="vid-name-montag-pic saiding-pic"></div>

            <div class="vid-text-montag">Сайдинг считается надежным, долговечным и современным отделочным материалом. Данные качества проявляют себя только в том случае, если отделка производится на высоком
                профессиональном
                уровне, с соблюдением всех
                рекомендаций производителя.
            </div>

        </div>

        <div class="vid-montag">
            <div class="vid-name-montag">Отделка цокольным сайдингом</div>

            <div class="vid-name-montag-pic saiding-cokol-pic"></div>
            <div class="vid-text-montag">Монтаж цокольного сайдинга проходит по тому же сценарию, что и монтаж классического винилового сайдинга и может быть выполнен с утеплением и без. Но тут играет роль - желание
                заказчика,
                если хочется произвести
                отделку дома под естественный камень, кирпич.
            </div>
        </div>

        <div class="vid-montag">
            <div class="vid-name-montag">Отделка блокхаусом</div>
            <div class="vid-name-montag-pic blokhouse-pic"></div>
            <div class="vid-text-montag">Отделка дома блокхаусом по своим качествам ничем не уступает отделке сайдингом. Главное различие - это внешний вид, но свойства материала и преимущества остаются на уровне, как и
                при
                отделке сайдингом. Главное соблюдать технологию монтажа.
            </div>

        </div>

        <div class="vid-montag">
            <div class="vid-name-montag">Монтаж кровли</div>
            <div class="vid-name-montag-pic krovlya-pic"></div>
            <div class="vid-text-montag">Монтаж кровли придаст не только эстетический вид вашему дому, но и благодаря современным материалам прослужит вам не один десяток лет, при этом не теряя внешнего вида и качеств,
                защищающих ваш дом от различных
                природных факторов, таких как: дождь, снег, град.
            </div>

        </div>

        <div class="vid-montag">
            <div class="vid-name-montag">Монтаж фиброцементного сайдинга</div>
            <div class="vid-name-montag-pic fibro-pic"></div>
            <div class="vid-text-montag">Фиброцементный сайдинг из волокон целлюлозы и цемента. Он по-настоящему надёжен и практичен. Обладает внешне нежностью песка, идеально подходит для облицовки дома, гармонично
                сочетается
                с ландшафтным дизайном. Он дешевле, чем камень, не менее красив, прочен.
            </div>

        </div>

        <div class="vid-montag">

            <div class="vid-name-montag">Монтаж металлического сайдинга</div>
            <div class="vid-name-montag-pic metallsaiding-pic"></div>
            <div class="vid-text-montag">Металлический сайдинг придает фасаду здания эстетичный внешний вид, скрывает внешние коммуникации (трубы, электропроводка), защищает фасад здания от осадков и механических
                воздействий,
                позволяет дополнительно
                утеплить помещение без потери площади внутри.
            </div>

        </div>

        <div class="vid-montag">

            <div class="vid-name-montag">Монтаж водосточной системы</div>
            <div class="vid-name-montag-pic vodostok-pic"></div>
            <div class="vid-text-montag">Водосточная система необходима для защиты и от попадания влаги на чердак дома, внутренние его части,

                фасад здания, во избежание разрушительного действия воды. Защищает фундамент и подвал

                от негативного воздействия осадков и защищает цокольную часть здания от переувлажнения

                и преждевременного разрушения.

                Мы делаем для вас такие условия, чтобы вы обращались только в нашу компанию, а мы с удовольствием выполнили все работы качественно и в срок

                Оставьте заявку на расчет стоимости отделки вашего дома
            </div>

        </div>
    </div>


</div>


<div class="yellback">
    <div class="container montag">

        <h2>Мы соблюдаем технологию монтажа</h2>
        <div class="montag-tekh">
            <img src="../asset/image/sborka2.png" width="500">
            <div class="pravila-montag-tekh">
                <div class="pravila-montag">
                    <div class="pravila-montag-header">
                        Обрешетка (профиля металл/дерево)
                    </div>
                    <div class="pravila-montag-body">
                        Правильная установка обрешетки обеспечивает долговечность конструкции и ровность поверхности.
                    </div>
                </div>
                <div class="pravila-montag">
                    <div class="pravila-montag-header">
                        Гидроизоляционная пленка
                    </div>
                    <div class="pravila-montag-body">
                        Не пропускают влагу не только на утеплитель, но и на стены дома, защищая от образования грибка.
                    </div>
                </div>
                <div class="pravila-montag">
                    <div class="pravila-montag-header">
                        Сайдинг
                    </div>
                    <div class="pravila-montag-body">
                        Находится в постоянном движении, в зависимости от температуры внешней среды. Только при соблюдении всех технологий вы получаете гарантию на сохранность формы конструкции.
                    </div>
                </div>
                <div class="pravila-montag">
                    <div class="pravila-montag-header">
                        Утеплитель
                    </div>
                    <div class="pravila-montag-body">
                        При правильном подборе утеплителя, а также правильности монтажа, вам будет обеспеченно сохранение тепла и материал не осыпется и не провиснет.
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>

<div class="container montag">


    <h2>При монтаже мы используем комплектующие всех брендов представленных на рынке</h2>


    <div class="montag-brends">

        <div class="montag-brends-row">
            <div>
                <img src="../asset/image/brands/alta-profil.jpg" width="120">
            </div>
            <div>
                <img src="../asset/image/brands/Docke.jpg" width="120">
            </div>
            <div>
                <img src="../asset/image/brands/GRANDLINE.jpg" width="120">
            </div>
            <div>
                <img src="../asset/image/brands/INTERPROFIL.jpg" width="120">
            </div>
            <div>
                <img src="../asset/image/brands/Katepal.jpg" width="120">
            </div>


        </div>
        <div class="montag-brends-row">


            <div>
                <img src="../asset/image/brands/Royal.jpg" width="120">
            </div>
            <div>
                <img src="../asset/image/brands/RUUKKI.jpg" width="120">
            </div>
            <div>
                <img src="../asset/image/brands/SHINGLAS.jpg" width="120">
            </div>
            <div>
                <img src="../asset/image/brands/Tegola.jpg" width="120">
            </div>
            <div>
                <img src="../asset/image/brands/Zodiac.jpg" width="120">
            </div>


        </div>

    </div>


    <p>Интересующие Вас материалы можно посмотреть в нашем <a href="/catalog">каталоге</a></p>




</div>


</div>
