<?

use AEngine\Orchid\Misc\Str;
use AEngine\Orchid\View;
use AEngine\Orchid\App;


/**
 * @var          $path
 */
?>

<style type="text/css">

  .content404 {
    background: url('/asset/image/404.png') center no-repeat #fff;
    background-size: contain;
    min-height: 400px;
  }

  .content {

    text-align: center;
    font-size: 20px;
  }

  .content a {
    text-decoration: none;
    color: #c10000;
  }
</style>

<?= View::fetch(App::getInstance()->path('view:Element/Nav.php'),
    [
        'path' => [],
    ]); ?>

<div class="breadcrumbs">
  <div class="path">
    <p><a href="/">Главная</a> / 404</p>
  </div>
</div> <!--end header-source-->
</div> <!--end header-->

<section>
  <main>
    <div class="content">
      <div class="content404">
      </div>
      <a href="/">Перейти к главной странице</a>

    </div>
  </main>

</section>


