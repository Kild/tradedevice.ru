<?

use AEngine\Orchid\Misc\Str;
use AEngine\Orchid\View;
use AEngine\Orchid\App;


/**
 * @var          $path
 * @var          $katalog
 */
?>

<style type="text/css">

    .content404 {
        background: url('/asset/image/font-selection-editor.png') center no-repeat #fff;
        background-size: contain;
        height: 100px;
    }

    .content {

        text-align: center;
        font-size: 20px;
    }

    .content a {
        text-decoration: none;
        color: #c10000;
    }
</style>

<?= View::fetch(App::getInstance()->path('view:Element/Nav.php'),
    [
        'path' => [],
        'menu' => \TradeMaster::getKatalog(),
    ]); ?>

<div class="breadcrumbs">
    <div class="path">
        <p><a href="/">Главная</a> / Готово</p>
    </div>
</div> <!--end header-source-->
</div> <!--end header-->

<section>
    <main>
        <div class="content">
            <div class="content404">
            </div>
            <a href="/">Перейти к главной странице</a>
            <br>
            <a href="/cup">ЦУП</a>

        </div>
    </main>

</section>


