<?

use AEngine\Orchid\Misc\Str;
use AEngine\Orchid\View;
use AEngine\Orchid\App;


/**
 * @var          $path
 * @var          $katalog
 */


$curl = curl_init();

curl_setopt($curl, CURLOPT_URL, 'https://krovlya-saiding.ru/kateg-yml/');
curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);

$result = curl_exec($curl);


?>

<style type="text/css">

    .content404 {
        background: url('/asset/image/font-selection-editor.png') center no-repeat #fff;
        background-size: contain;
        height: 100px;
    }

    .content {

        text-align: center;
        font-size: 20px;
    }

    .content a {
        text-decoration: none;
        color: #c10000;
    }


    .ymlFiles{

        display: flex;
        flex-wrap: wrap;
        flex-direction: column;
        align-items: center;
    }


    .ymlFiles li {
        display: flex;
        justify-content: space-between;
        width: 50%;
    }

</style>

<?= View::fetch(App::getInstance()->path('view:Element/Nav.php'),
    [
        'path' => [],
        'menu' => \TradeMaster::getKatalog(),
    ]); ?>

<div class="breadcrumbs">
    <div class="path">
        <p><a href="/">Главная</a> / ЦУП</p>
    </div>
</div> <!--end header-source-->
</div> <!--end header-->
<noindex>
<section>
    <main>
        <div class="content">
            <div>

                <h1>Центр управления полетами</h1>

<br>
                <ul class="ymlFiles">

                    <?

                    preg_match_all('/<(?:[aA][\s]+[hH][rR][eE][fF]|[iI][mM][gG][\s]+.*[\s]+[sS][rR][cC])[\s]*=[\s]*[\'"]([^\s">]+)[\'"]/', $result, $res);



                    for ($i = count($res)-1; $i >= 0; $i--) {

                        for ($k = count($res[$i])-1; $k >= 1; $k--) {

                            $ymlFile = $res[$i][$k];

                            if  (($ymlFile !='../') && !!$ymlFile && !strpos($ymlFile, 'href')){

                                $yml= str_replace('-yml.xml' , '', $ymlFile);

                    ?>
                    <li>


                        <a target="_blank" href='/kateg-yml/<?=  $ymlFile ?>'><?= $ymlFile ?></a>


                        <a rel="nofollow" href="/yml?kateg=<?=    $yml ?>">Обновить</a>


                    </li>

                                <?

                            }
                        }

                    }


                    ?>

                </ul>

            </div>

            <br><br><br><br>
            --------------------------------<br>
            <a href="/">Перейти к главной странице</a>
            <br>
            <a href="/yml.xml" target="_blank">yml.xml</a>
            <br>
            <a href="/sitemap.xml" target="_blank">sitemap.xml</a>

        </div>
    </main>

</section>
</noindex>

