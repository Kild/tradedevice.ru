<?
use AEngine\Orchid\App;
use AEngine\Orchid\Misc\Str;
use AEngine\Orchid\View;


/**
 * @var  Model\News $model
 * @var   array $pathArray
 */
?>
<?= View::fetch(App::getInstance()->path('view:Element/Nav.php'),
    [
        'path' => $pathArray,
    ]); ?>

<div class="breadcrumbs">
    <div class="path">
        <p><a href="/">Главная</a> / <a href="/news">Полезно знать</a> / <?= $model->get('title'); ?></p>
    </div>
</div> <!--end header-source-->
</div> <!--end header-->

<section>
    <main>
        <div class="content">
            <?
            if (!empty($model)) {
                ?>
                <h1><?= $model->get('title'); ?></h1>
                <?
                if ($model->get('video')) {
                    ?>
                    <div class="videoRecord">
                    <iframe src="<?= str_replace("/watch?v=", "/embed/", $model->get('video')) ?>?rel=0" >
                    </iframe>
                    </div>
                    <?
                }
                ?>
                <?= Str::unEscape($model->get('text')); ?>
                <div class="backToNews"><a href="/news">&larr; назад</a></div>
                <?
            }
            ?>
        </div>
    </main>
</section>

