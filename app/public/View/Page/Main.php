<?
/**
 * @var   $catalog
 * @var                            $linkZvena ,
 * @var                            $nameZvena ,
 */

use AEngine\Orchid\App;
use AEngine\Orchid\View;

$app = App::getInstance();
?>

<?= View::fetch(App::getInstance()->path('view:Element/Nav.php'),
    [
        'path' => [],
        'menu' => $catalog,
    ]); ?>

</div> <!--end header-->

<div class="content item main">
    <div class="section">

  
      

    </div>
</div>
</div>

<div class="five-step-wrapper">
    <div class="five-step-inner">
       
      


    </div>
</div>


<div class="container">
    <div class="content item main">
        <div class="section">


            <h2>Каталог строительного материала для Вашего ремонта</h2>

          
        </div>


        <h2 class="mainp">Наш офис</h2>
      


        <p class="mainp">Мы оперативно рассчитаем стоимость необходимого количества материала и стоимость монтажа на Ваш дом.</p>
        <p class="mainp">Ближайшие метро и районы: Бульвар Дмитрия Донского, Бутово, Щербинка, Воскресенское и Чертаново.</p>
        <div style="clear: both;"></div>


    </div>
</div>


<div class="mainImg">
    <div class="parallax-slider">
        <img src="/asset/image/zabor460.jpg" srcset="/asset/image/zabor768.jpg 768w, /asset/image/zabor1170.jpg 1170w" sizes="100vw">
    </div>
</div>

<div class="content item main">
    <div class="prichini" id="advantages">
        <div class="container">
            <h2>О компании Фронт Хаус</h2>
            <div class="okompanii">

                <p>Мы команда профессионалов, искренне любим и знаем свое дело. В сфере кровли и фасада опыт более <span class="boldmain">5 лет</span>.</p>

                <p>Консультации в выборе материалов, произведем монтаж. Мы ответственно исполним возложенные на нас обязательства.</p>

                <p>Реализовано более <span class="boldmain">100 проектов</span> в Московской области с нашим участием в поставках и монтаже.</p>

            </div>          


        </div>
    </div>
 </div>



