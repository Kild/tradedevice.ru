<?
/**
 * @var Model\Page $model
 * @var          $path
 *  @var          $katalog
 */
use AEngine\Orchid\Misc\Str;
use AEngine\Orchid\View;
use AEngine\Orchid\App;

?>

<?= View::fetch(App::getInstance()->path('view:Element/Nav.php'),
    [
        'path' => explode('/', $path),
        'menu' => $katalog,



    ]); ?>

<div class="breadcrumbs">
    <div class="path">
        <p><a href="/">Главная</a> / <?= $model->get('title'); ?></p>
    </div>
</div> <!--end header-source-->
</div> <!--end header-->

<section>

    <main>
        <div class="content">
            <h1><?= $model->get('title'); ?></h1>
            <?= Str::unEscape($model->get('content')); ?>
        </div>
    </main>

</section>


