<?
use AEngine\Orchid\App;
use AEngine\Orchid\Misc\Str;
use AEngine\Orchid\View;


/**
 * @var  $list
 * @var   array $pathArray
 */
?>
<?= View::fetch(App::getInstance()->path('view:Element/Nav.php'),
    [
        'path' => $pathArray,
        'menu' => $katalog,
    ]); ?>

<div class="breadcrumbs">
    <div class="path">
        <p><a href="/">Главная</a> / Полезно знать</p>
    </div>
</div> <!--end header-source-->
</div> <!--end header-->

<section>

    <main>

        <div class="container">
            <?
            if (count($pathArray) == 2) {
                ?> <h1>Полезно знать</h1> <?
            }
            ?>
            <div class="news-list">
                <?
                if (!empty($list)) {

                    foreach ($list as $index => $row) {
                        ?>
                        <div class="news-item">
                            <div class="news-img">
                                <?= (!!$row["img"]) ? '<div  class="news-image" style="background-image:url(' . $row['img'] . ');" ></div>' : '' ?>
                            </div>
                            <div class="news-desc">
                                <h2><a href="/news/<?= $row["url"]; ?>"><?= $row["title"]; ?></a></h2>
                                <p><?= Str::unEscape($row["announce"] ? $row["announce"] : Str::truncate($row["text"], 200)); ?></p>
                                <a class="news-link" href="/news/<?= $row["url"]; ?>"><?= (!!$row["video"]) ? 'смотреть' : 'читать' ?> &rarr;</a>
                            </div>
                        </div>
                        <?
                    }
                }
                ?>

            </div>

        </div>

    </main>

</section>

