<?

use AEngine\Orchid\App;
use AEngine\Orchid\View;

/**
 * @var   $nomerZakaza
 *
 */

?>

<?= View::fetch(App::getInstance()->path('view:Element/Nav.php'),
    [
        'path' => [],
        'menu' => \TradeMaster::getKatalog(),
    ]); ?>

<div class="breadcrumbs">
    <div class="path">
        <p><a href="/">Главная</a> / Заявка / Ваша заявка <?= $nomerZakaza ?> принята</p>
    </div>
</div> <!--end header-source-->
</div> <!--end header-->

<section>
    <main>
        <div class="container">
            <div class="content">
                <script>
                    simpleCart.empty();
                </script>
                <h1>Спасибо!</h1>
                <h2>Ваша заявка <?= $nomerZakaza ?> принята</h2>

                <p>Копия заявки отправлена на указанный адрес электронной почты.</p>
                <p>В ближайшее время наши менеджеры с вами свяжутся для согласования заявки.</p>

                <br>
                <p>Перейти в <a href='/catalog'>каталог</a>. Перейти на <a href='/'>главную</a></p>
            </div>

        </div>
    </main>
</section>
