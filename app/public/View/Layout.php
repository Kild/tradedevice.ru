<?php

use AEngine\Orchid\App;
use AEngine\Orchid\Misc\Asset;


$app = App::getInstance();
?><!DOCTYPE html>
<html lang="ru">
<head>
    <title><?= $title ?? ''; ?></title>
    <meta charset="utf-8"/>
    <meta name="description" content="<?= $description ?? '102tools' ?>">
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="#FFD634"/>
    <meta name="theme-color" content="#FFD634">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
    <!--    <meta name="viewport" content="initial-scale=1.0, width=device-width">-->
    <link rel="icon" type="image/png" href="/favicon.png"/>
    <?= ($canon != '') ? '<link rel="canonical" href="' . $canon . '" />' : '' ?>

    <? if ($app->get('debug')) { ?>
        <script type="text/javascript">
            var less = {env: 'development'};
        </script>
    <? } ?>

    <?= Asset::resource() . PHP_EOL; ?>

</head>
<body>
<div id="p_prldr" style="display: none;">
    <div class="contpre"><span class="svg_anm"></span></div>
</div>

<div id="cartIcon">

    <a href="/zayavka"><i class="fa fa-shopping-cart"></i></a>
</div>


<div class="container">
    <div class="header">

        <div class="top">
            <div class="menuButton">
                <i class="fa fa-bars" aria-hidden="true" id="menuButton"></i>

                <i class="fa fa-search" aria-hidden="true" id="searchButton"></i>

            </div> <!--end menuButton-->

            <div class="search" id="searchForm">
                <script type="text/javascript">window.ya_site_path = {serp: "//site.yandex.ru/search/site/catalog/"};</script>
                <div class="ya-site-form ya-site-form_inited_no" onclick="return {'action':'https://krovlya-saiding.ru/poisk',
                'arrow':false,'bg':'transparent','fontsize':'15','fg':'#000000','language':'ru','logo':'rb','publicname':'','suggest':true,
                'target':'_self','tld':'ru','type':2,'usebigdictionary':false,'searchid':2321661,'input_fg':'#000000',
                'input_bg':'#FFFFFF','input_fontStyle':'normal','input_fontWeight':'normal','input_placeholder':'Поиск по товарам',
                'input_placeholderColor':'#000000','input_borderColor':'#cbcbcb','is_catalog':true}">
                    <form action="//site.yandex.ru/search/site/catalog/" method="get" target="_self" accept-charset="utf-8"><input type="hidden" name="searchid" value="2321661"/><input type="hidden" name="l10n"
                                                                                                                                                                                         value="ru"/><input
                                type="hidden" name="reqenc" value=""/><input type="search" name="text" value=""
                                                                             style="outline: 0 !important;font-size: 17px !important;height: 46px !important;padding: 6px 10px 7px !important;margin: 10px 0 5px 0 !important;border: 1px solid #FFD634;border-radius: 3px !important;"/><input
                                type="submit" value="Найти"
                                style="outline: 0 !important;font-size: 15px !important;background: #fff !important;color: #000 !important;height: 32px !important;padding: 6px 15px 7px !important;border: 1px solid #cbcbcb;margin: 0 0 0 -1px;border-radius: 0 3px 3px 0;"/>
                    </form>
                </div>
                <style type="text/css">.ya-page_js_yes .ya-site-form_inited_no {
                        display: none;
                    }

                    .ya-site-form__input-text:focus, .ya-site-form__submit:focus {
                        border-color: #d0ba65 !important;
                        box-shadow: 0 0 10px #fc0 !important;
                    }</style>
                <script type="text/javascript">(function (w, d, c) {
                        var s = d.createElement('script'), h = d.getElementsByTagName('script')[0], e = d.documentElement;
                        if ((' ' + e.className + ' ').indexOf(' ya-page_js_yes ') === -1) {
                            e.className += ' ya-page_js_yes';
                        }
                        s.type = 'text/javascript';
                        s.async = true;
                        s.charset = 'utf-8';
                        s.src = (d.location.protocol === 'https:' ? 'https:' : 'http:') + '//site.yandex.net/v2.0/js/all.js';
                        h.parentNode.insertBefore(s, h);
                        (w[c] || (w[c] = [])).push(function () {
                            Ya.Site.Form.init()
                        })
                    })(window, document, 'yandex_site_callbacks');</script>

            </div> <!--end search-->
            <div class="logo">
                <a href="/">
                    <map name="knopka">
                        <area shape="rect" coords="0,70,190,105" href="/catalog/krovelnye-materialy" alt="Кровельные материалы">
                        <area shape="rect" coords="191,70,340,105" href="/catalog/fasadnye-materialy" alt="Фасадные материалы">
                        <area shape="rect" coords="341,70,600,105" href="/catalog/zabory-i-komplektuyushhie" alt="Заборы и комплектующие">
                        <img src="/asset/image/icon/logo_horizontal_signature.svg" width="200" alt="Логотип ФронтХаус" usemap="#knopka">
                    </map>

                </a>
            </div> <!--end logo-->
            <div class="contact">
                <a href="tel:+74951994235" target="_blank">
                    <i class="fa fa-phone"></i>
                </a>
                <p class="topTel">8 (495) 199-4235</p>
                <a href="mailto:info@krovlya-saiding.ru" target="_blank">
                    <i class="fa fa-envelope"></i>
                </a>
                <p class="topMail">info<i>@</i>krovlya-saiding<i>.</i>ru</p>
                <p class="topAdress">г.Москва, Куликовская улица, 12 - 507А</p>
            </div> <!--end contact-->



        </div> <!--end header-top-->

        <?= $content; ?>

    </div>
    <div class="footer">
        <div class="container">

            <div class="containerFooter">

                <div class="topFooter" itemscope itemtype="http://schema.org/Organization">
                    <div class="footLogo" style="background: #ffffff;float: left; margin-top: 20px">
                        <img src="/asset/image/icon/logo_vertical_savefields.png" height="100">
                    </div>
                    <div class="phone" itemprop="telephone"><p>8 (495) 199-4235</p></div>

                    <div class="phone"><p>Москва, Куликовская, 12-507А</p></div>
                    <div class="phone"><p>info<i>@</i>krovlya-saiding<i>.</i>ru</p></div>


                    <meta itemprop="name" content="Фронт-хаус">
                    <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">

                        <meta itemprop="addressLocality" content="Москва">
                        <meta itemprop="streetAddress" content="Куликовская улица, 12 - 507А">
                    </div>

                    <div itemscope itemtype="http://schema.org/LocalBusiness">
                        <p class="timeFoot" itemprop="openingHours" datetime="Mo-Fr 9:00−18:00">пн-пт с 9.00 до 18.00</p>
                        <meta itemprop="name" content="Фронт-хаус">
                        <meta itemprop="telephone" content="+7 (495) 199-4235">
                        <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                            <meta itemprop="addressLocality" content="Москва">
                            <meta itemprop="streetAddress" content="Куликовская улица, 12 - 507А">
                        </div>
                    </div>


                </div> <!--end footer-top-->


                <ul class="catalogFooter">
                 
                </ul>
            </div>
            <!--end footer-bottom-->
        </div>
    </div> <!--end footer-->

</html>
