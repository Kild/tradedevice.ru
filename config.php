<?php

return [
    // debug mode
    'debug'     =>0,

    // app list
    'app.list'  => [
        'public',
    ],

    // secret keystring
    'secret'    => 'ufatest1-secret-string',

    // base app dir
    'base_dir'  => __DIR__,

    // base host name
    'base_host' => 'ufatest1.u4et.ru',

    // base port
    'base_port' => 80,

    "database" => [
        [
            "dsn"      => "mysql:host=localhost;dbname=ufatest1",
            "username" => "ufatest1",
            "password" => "P8t6K8n5",
            "options"  => [
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
            ],
        ],
    ],


    // memcache connection server list
    'memory'   => [
        [
            'host' => 'localhost',
            'port' => 11211,
        ],
    ],

    'TradeMaster' => [
        'version'    => 1,
        'host'       => 'https://api.trademaster.pro',
        'key'        => 'pJC7AVLb3Yj7kRdf0O97OvKo1Ggby0DkO6tILixA',
        'cache'      => true,
        'cache_time' => 36000,
        'cache_tag'  => 'TDDB2',
        "domain"     => "trademaster.pro",
        "folder"     => "TDDB2",
        "paginator"  => 30,
        "currency"   => "RUB",
        "lastView"   => 10,
        "struct"     => 0,

        "storage"    => 1,
        "legal"      => 1,
        "checkout"   => 1,
        "contractor" => 3,
        "scheme"     => 6,

        "tm_userID"  => 1170,
        "mail"       => "ilshatkin@mail.ru",

        "mail_host"    => "smtp.yandex.ru",
        "mail_port"    => 465,
        "mail_user"    => "mail@krovlya-saiding.ru",
        "mail_pass"    => "krovlya-saidingkrovlya-saiding",
        "mail_from"    => "mail@krovlya-saiding.ru",
        "mail_name"    => "krovlya-saiding",

        "mail_charset" => "utf-8",
        "mail_ssl"     => true,
        "mail_subject" => "krovlya-saiding.ru: Ваш заказ номер {cart_num} принят к исполнению",

        "yml_shop" => "krovlya-saiding",
        "yml_company" => "ИП Кильдияров И.Р.",
        "yml_price" => "1200",
        "yml_currency" => "RUB",
        "yml_host" => "https://krovlya-saiding.ru",



    ],

    // module folder list
    'modules'     => [
        __DIR__ . '/src/Module',
    ],
];
